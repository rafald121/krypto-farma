import json

import requests
import urllib

URL_MARKET_PAIRS = 'https://pro-api.coinmarketcap.com/v1/exchange/market-pairs/latest'
URL_CRYPTOCURRENCY_LISTING_HISTORICAL = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/historical'
URL_CRYPTOCURRENCY_LISTING_LATEST = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
URL_CRYPTOCURRENCY_LISTING_INFO = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/info'
API_KEY = '44d4930a-c12c-40d9-a3ef-7808afd05cfa'


def get_listing_latest_data(start=1, limit=1000, convert='USD'):
    headers = {
        'X-CMC_PRO_API_KEY': API_KEY,
        'Accept': 'application/json'
    }
    params = {
        'start': start,
        'limit': limit,
        'convert': convert,
    }
    url_encoded_params = urllib.parse.urlencode(params)
    url = f'{URL_CRYPTOCURRENCY_LISTING_LATEST}?{url_encoded_params}'
    response = requests.get(url, headers=headers)
    response_content = response.content
    response_data = json.loads(response_content)
    return response_data['data']


def get_coins_metadata(ids):
    headers = {
        'X-CMC_PRO_API_KEY': API_KEY,
        'Accept': 'application/json'
    }
    params = {
        'id': ids,
    }
    url_encoded_params = urllib.parse.urlencode(params)
    url = f'{URL_CRYPTOCURRENCY_LISTING_INFO}?{url_encoded_params}'
    response = requests.get(url, headers=headers)
    response_content = response.content
    response_data = json.loads(response_content)
    return response_data['data']