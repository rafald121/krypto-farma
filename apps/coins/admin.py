from django.contrib import admin
from django.shortcuts import render

from actions.models import Action
from coins.models import Coin, Ticker


class CoinAdmin(admin.ModelAdmin):
    pass

class TickerAdmin(admin.ModelAdmin):
    pass



admin.site.register(Coin, CoinAdmin)
admin.site.register(Ticker, TickerAdmin)
