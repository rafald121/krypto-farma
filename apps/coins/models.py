from django.db import models


# Create your models here.
class Coin(models.Model):

    symbol = models.CharField(max_length=12)
    quality_level = models.FloatField(null=True)
    capitalization = models.BigIntegerField(null=True)
    is_up_down_possibility = models.BooleanField(null=True)

    def __str__(self):
        return self.symbol


class Ticker(models.Model):
    base = models.ForeignKey(Coin, on_delete=models.DO_NOTHING, related_name='base')
    quote = models.ForeignKey(Coin, on_delete=models.DO_NOTHING, related_name='quote')

    class Meta:
        unique_together = [['base', 'quote']]

    @property
    def name(self):
        return f'{self.base.symbol}{self.quote.symbol}'

    def __str__(self):
        return self.name