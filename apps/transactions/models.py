# from django.db import models
#
# from utils.models import Timestampable
#
#
# class Transaction(Timestampable):
#     pass
#
#
# class TransactionBinance(Transaction):
#     BUY = 'Buy'
#     SELL = 'Sell'
#
#     side_choices = (
#         (BUY, 'Buy'),
#         (SELL, 'Sell')
#     )
#
#     symbol = models.CharField(max_length=12)
#     symbol_ticker = models.CharField(max_length=12)
#     is_bought_successfully = models.BooleanField()
#     quantity_ask = models.FloatField()
#     quantity_processed = models.FloatField(blank=True, null=True)
#     price_ask = models.FloatField(
#         null=True,
#         help_text='Null true because we buy market'
#     )
#     price_processed = models.FloatField(
#         blank=True, null=True
#     )
#     status_processed = models.CharField(
#         blank=True, null=True, max_length=64
#     )
#     side = models.CharField(choices=side_choices, max_length=64)
#     type = models.CharField(max_length=64)