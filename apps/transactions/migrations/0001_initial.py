# Generated by Django 3.1.7 on 2021-04-20 20:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, help_text='Creation date')),
                ('updated_at', models.DateTimeField(auto_now=True, help_text='Update date')),
            ],
            options={
                'get_latest_by': 'updated_at',
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TransactionBinance',
            fields=[
                ('transaction_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='transactions.transaction')),
                ('coin', models.CharField(max_length=12)),
                ('coin_symbol_ticker', models.CharField(max_length=12)),
                ('is_bought_successfully', models.BooleanField()),
                ('quantity_ask', models.FloatField()),
                ('quantity_processed', models.FloatField(blank=True, null=True)),
                ('price_ask', models.FloatField()),
                ('price_processed', models.FloatField(blank=True, null=True)),
                ('status_processed', models.CharField(blank=True, max_length=64, null=True)),
                ('side', models.CharField(choices=[('Buy', 'Buy'), ('Sell', 'Sell')], max_length=64)),
                ('type', models.CharField(max_length=64)),
            ],
            options={
                'get_latest_by': 'updated_at',
                'abstract': False,
            },
            bases=('transactions.transaction',),
        ),
    ]
