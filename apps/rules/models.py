from django.db import models

from actions.models import Action
from coins.models import Coin, Ticker
from utils.models import Timestampable


class Rule(Timestampable):

    name = models.CharField(max_length=64)
    description = models.CharField(max_length=256, null=True, blank=True)

    ticker = models.ManyToManyField(Ticker)

    date_valid_to = models.DateTimeField(null=True)
    days_valid = models.PositiveIntegerField(null=True)

    action = models.ManyToManyField(Action)


class RuleChange(Rule):
    DIRECTION_UP = 10
    DIRECTION_DOWN = 20

    direction_choices = (
        (DIRECTION_UP, 'Up'),
        (DIRECTION_UP, 'Down'),
    )

    change_within_time = models.BigIntegerField(
        null=True, blank=True,
        help_text='Provide time in seconds in which change occurred'
    )
    direction = models.PositiveSmallIntegerField(choices=direction_choices)

    def is_condition_met(self):
        pass


class RuleChangeRelative(RuleChange):
    percentage = models.FloatField()


class RuleChangeAbsolute(RuleChange):
    value = models.FloatField()


class RuleWallet(Rule):
    """Rule that tell us that we have some coin in wallet"""

    def is_coin_in_wallet(self):
        pass
