from django.contrib import admin

# Register your models here.
from rules.models import Rule, RuleChange, RuleChangeRelative, RuleChangeAbsolute, RuleWallet


class RuleAdmin(admin.ModelAdmin):
    pass


admin.site.register(Rule, RuleAdmin)


class RuleChangeAdmin(admin.ModelAdmin):
    pass


admin.site.register(RuleChange, RuleChangeAdmin)


class RuleChangeRelativeAdmin(admin.ModelAdmin):
    pass


admin.site.register(RuleChangeRelative, RuleChangeRelativeAdmin)


class RuleChangeAbsoluteAdmin(admin.ModelAdmin):
    pass


admin.site.register(RuleChangeAbsolute, RuleChangeAbsoluteAdmin)


class RuleWalletAdmin(admin.ModelAdmin):
    pass


admin.site.register(RuleWallet, RuleWalletAdmin)
