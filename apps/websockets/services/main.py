from binance.client import Client
from binance.websockets import BinanceSocketManager
from krypto_farma.settings_binance import BINANCE_API_KEY, BINANCE_SECRET_KEY


class Websocket:
    def __init__(self):
        self.client = Client(BINANCE_API_KEY, BINANCE_SECRET_KEY)
        self.socket_manager = BinanceSocketManager(self.client)


class WebsocketKline(Websocket):
    """
    {
   "e":"kline",
   "E":1614888768228,
   "s":"BTCUSDT",
   "k":{
      "t":1614888720000,
      "T":1614888779999,
      "s":"BTCUSDT",
      "i":"1m",
      "f":684530479,
      "L":684531296,
      "o":"47547.80000000",
      "c":"47562.42000000",
      "h":"47588.34000000",
      "l":"47530.01000000",
      "v":"21.71985100",
      "n":818,
      "x":false,
      "q":"1032869.54223326",
      "V":"10.38939800",
      "Q":"494042.50339795",
      "B":"0"
   }

    """

    def start(self, symbol, interval):
        self.socket_manager.start_kline_socket(
            symbol=symbol, callback=self.callback, interval=interval,
        )

    def callback(self):
        pass


class WebksocketSymbolTicker(Websocket):
    """
    {
       "e":"24hrTicker",
       "E":1614889229461,
       "s":"BTCUSDT",
       "p":"-3053.96000000",
       "P":"-5.982",
       "w":"49578.93999603",
       "x":"51049.99000000",
       "c":"47996.03000000",
       "Q":"0.09325900",
       "b":"47991.41000000",
       "B":"0.10000000",
       "a":"47998.23000000",
       "A":"0.26000000",
       "o":"51049.99000000",
       "h":"51773.88000000",
       "l":"47500.00000000",
       "v":"82543.24258700",
       "q":"4092406471.29872800",
       "O":1614802829449,
       "C":1614889229449,
       "F":682233121,
       "L":684543021,
       "n":2309901
    }
    """
    def start(self, symbol):
        self.socket_manager.start_symbol_ticker_socket(
            symbol=symbol, callback=self.callback
        )

    def callback(self):
        pass