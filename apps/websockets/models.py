from django.db import models


# TODO multiple channels
# Create your models here.
class Websocket(models.Model):
    KLINE_SOCKET = 10
    KLINE_SOCKET_LABEL = 'kline socket'

    WEBSOCKET_TYPES = (
        (KLINE_SOCKET, KLINE_SOCKET_LABEL),

    )

    symbol = models.CharField(max_length=12)
    websocket_type = models.PositiveSmallIntegerField(choices=WEBSOCKET_TYPES)
    active = models.BooleanField(null=True)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=128)


# TODO consider distinct class per websocket type
class WebsocketKline(Websocket):
    pass
