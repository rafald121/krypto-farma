from django.contrib import admin
from django.shortcuts import render

from websockets.models import Websocket, WebsocketKline


class WebsocketAdmin(admin.ModelAdmin):
    pass


class WebsocketKlineAdmin(admin.ModelAdmin):
    pass


admin.site.register(Websocket, WebsocketAdmin)
admin.site.register(WebsocketKline, WebsocketKlineAdmin)