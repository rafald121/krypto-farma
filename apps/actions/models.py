from django.db import models
from utils.models import Timestampable


# TODO Add ActionTemplate

class Action(Timestampable):

    ACTION_BUY = 10
    ACTION_BUY_LABEL = 'Action buy'

    ACTION_CHOICES = (
        (ACTION_BUY, ACTION_BUY_LABEL),
    )

    date_triggered = models.DateTimeField()
    action = models.PositiveSmallIntegerField(choices=ACTION_CHOICES)


class ActionBuy(Action):
    pass


class ActionSell(Action):
    pass



