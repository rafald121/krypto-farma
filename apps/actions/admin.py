from django.contrib import admin
from django.shortcuts import render

from actions.models import Action


class ActionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Action, ActionAdmin)
