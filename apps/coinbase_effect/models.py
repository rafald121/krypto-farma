# import logging
# from typing import List
#
# from api_twitter.utils import COINBASE, COINBASE_PRO
# from django.db import models
#
# from coinbase_effect.services.potential_growth_calculator import PotentialGrowthCalculator
# from transactions.models import TransactionBinance
# from dataclasses import dataclass
#
#
# logger = logging.getLogger(__name__)
#
#
# @dataclass
# class CoinbaseEffectConfig:
#     amount_to_invest: str
#
#
# class CoinbaseEffectNews(models.Model):
#     BLOG = 'coinbase_blog'
#
#     source_choices = (
#         (BLOG, 'Blog'),
#         (COINBASE, 'Twitter coinbase'),
#         (COINBASE_PRO, 'Twitter coinbase pro'),
#     )
#
#     NEW = 10
#     ANALYSED = 15
#     BOUGHT = 20
#     SELL_ORDER = 30
#     SOLD = 40
#     FAILED = 100
#
#     status_choices = (
#         (NEW, 'News appeared'),
#         (ANALYSED, 'Analysed'),
#         (BOUGHT, 'Bought'),
#         (SELL_ORDER, 'Sell order placed'),
#         (SOLD, 'Sold'),
#         (FAILED, 'Failed'),
#     )
#     config = models.ForeignKey(CoinbaseEffectConfig, on_delete=models.PROTECT)
#
#     source = models.CharField(choices=source_choices, max_length=64)
#     status_choices = models.PositiveSmallIntegerField(status_choices, default=NEW)
#     date_published = models.DateTimeField(auto_now_add=True)
#     content = models.CharField(max_length=1024)
#     content_coins = models.CharField(  # TODO change to JSON ?
#         max_length=1024, blank=True, null=True,
#         help_text='Coins that was found in source text content'
#     )
#     url = models.URLField(
#         blank=True, null=True,
#         help_text='URL of news. Saved manually'
#     )
#     date_published_twitter = models.DateTimeField(
#         blank=True, null=True,
#         help_text='Date news published on Twitter'
#     )
#     date_published_blog = models.DateTimeField(
#         blank=True, null=True,
#         help_text='Date news published on Blog'
#     )
#
#     def set_remaining_data(
#         self,
#         tweet_data_text_coins: List,
#     ) -> None:
#         self.content_coins = ', '.join(tweet_data_text_coins)
#         self.save()
#
#     @classmethod
#     def get_source_from_screen_name(cls, screen_name):
#         if screen_name == COINBASE:
#             return COINBASE
#         elif screen_name == COINBASE_PRO:
#             return COINBASE_PRO
#         logger.error('Cannot assign proper source from %s', screen_name)
#         return None
#
#     def get_coins_related(self):
#         return self.coins.all()
#
#     def has_bought_successfully(self):
#         return self.get_coins_related().filter(transaction__is_bought_successfully=True).exists()
#
#     def get_bought_coins(self):
#         return self.get_coins_related().filter(transaction__side=TransactionBinance.BUY)
#
#     def should_notify_by_email(self):
#         """
#         Based on relation data return if should notify
#         :return:
#         """
#         return True
#
#     def get_data_to_notify_by_email(self):
#         """
#         Based on relation data return data
#         """
#         return {}
#
#
# class CoinbaseEffectCoin(models.Model):
#     FOUND_IN_TEXT = 5
#     DECIDED_TO_BUY = 10
#     BOUGHT = 20
#     SELL_ORDER = 30
#     SOLD = 40
#     REJECTED = 50
#     FAILED = 10
#
#     status_choices = (
#         (FOUND_IN_TEXT, 'Found in text'),
#         (DECIDED_TO_BUY, 'Decided to buy'),
#         (BOUGHT, 'Bought'),
#         (SELL_ORDER, 'Sell order'),
#         (SOLD, 'Sold'),
#         (REJECTED, 'Rejected'),
#         (FAILED, 'Failed'),
#     )
#
#     coinbase_effect_news = models.ForeignKey(
#         CoinbaseEffectNews, on_delete=models.DO_NOTHING, related_name='coins'
#     )
#     transaction = models.ForeignKey(TransactionBinance, on_delete=models.DO_NOTHING, null=True)
#
#     symbol = models.CharField(max_length=10)
#     symbol_ticker = models.CharField(max_length=15)
#     symbol_lot = models.CharField(max_length=15)
#
#     status = models.PositiveSmallIntegerField(choices=status_choices, default=FOUND_IN_TEXT)
#     is_chosen = models.BooleanField(
#         default=False, help_text='Was chosen to be bought? '
#     )
#
#     pump_percentage = models.FloatField(blank=True, null=True)
#     duration = models.IntegerField(
#         help_text='Pump duration in minutes', blank=True, null=True
#     )
#
#     ranking = models.IntegerField(blank=True, null=True)
#     market_cap = models.IntegerField(blank=True, null=True)
#
#     price_before = models.FloatField(
#         blank=True, null=True,
#         help_text='Price before pump, before coinbase effect notification'
#     )
#     price_bought = models.FloatField(blank=True, null=True)
#     price_sell = models.FloatField(blank=True, null=True)
#     price_sell_percentage = models.FloatField(blank=True, null=True)
#     price_sold = models.FloatField(blank=True, null=True)
#
#     quantity_ask = models.FloatField(blank=True, null=True)
#
#     @classmethod
#     def save_coins_calculated(
#         cls,
#         coinbase_effect_news: CoinbaseEffectNews,
#         coins_calculated: List[PotentialGrowthCalculator],
#     ):
#         coins_created = []
#         for coin in coins_calculated:
#             coin_object = CoinbaseEffectCoin.objects.create(
#                 coinbase_effect_news=coinbase_effect_news,
#                 is_choose=True,
#                 symbol=coin.coin,
#                 symbol_ticker=coin.coin_symbol_ticker_usdt,
#                 symbol_lot=coin.symbol_lot,
#                 ranking=coin.ranking,
#                 market_cap=coin.market_cap,
#                 price_before=coin.price,
#             )
#             coins_created.append(coin_object)
#         return coins_created
#
#
# class CoinbaseEffectTransaction(models.Model):
#     """
#     We consider transaction as whole process:
#     from buying few coins (no matter if successfully)
#     to selling it (no matter if successfully
#     """
#
#     NEW = 10
#     BOUGHT_SUCCEFULLY = 20
#     BOUGHT_AT_LEAST_ONE_FAILED = 23
#     BOUGHT_ALL_FAILED = 24
#     SOLD_SUCCESFULLY = 30
#     SOLD_ALL_FAILED = 30
#
#     status_choices = (
#         (NEW, 'NEW'),
#         (BOUGHT_SUCCEFULLY, 'BOUGHT_SUCCEFULLY'),
#         (BOUGHT_AT_LEAST_ONE_FAILED, 'BOUGHT_AT_LEAST_ONE_FAILED'),
#         (BOUGHT_ALL_FAILED, 'BOUGHT_ALL_FAILED'),
#         (SOLD_SUCCESFULLY, 'SOLD_SUCCESFULLY'),
#         (SOLD_ALL_FAILED, 'SOLD_ALL_FAILED'),
#     )
#
#     news = models.ForeignKey(CoinbaseEffectNews, on_delete=models.DO_NOTHING)
#     transactions = models.ManyToManyField(TransactionBinance, related_name='coinbase_effect_transactions')
#     status = models.PositiveSmallIntegerField(choices=status_choices, default=NEW)
#     amount_to_invest = models.FloatField()
#
#     def get_coins_to_buy(self):
#         return self.coins_potential.filter(status=CoinbaseEffectCoin.DECIDED_TO_BUY)
#
#     def get_bought_coins(self):
#         return self.coins_potential.filter(transaction__side=TransactionBinance.BUY)
#
#     def has_bought_successfully(self):
#         return self.coins_potential.filter(transaction__is_bought_successfully=True).exists()
#
#     def should_notify_by_email(self):
#         """
#         Based on relation data return if should notify
#         :return:
#         """
#         return True
#
#     def get_data_to_notify_by_email(self):
#         """
#         Based on relation data return data
#         """
#         return {}