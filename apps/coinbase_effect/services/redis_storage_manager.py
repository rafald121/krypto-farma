from datetime import date
import json
import logging
from urllib.error import HTTPError

import redis

from api_binance.utils import (
    get_available_coins,
    get_coins_up_levarage,
    get_coins_down_levarage,
    get_available_coins_price,
    get_symbol_lots, get_symbol_min_notional,
)
from api_coinmarketcap.main import get_listing_latest_data
from coinbase_effect.services.constants import COINBASE_AVAILABLE_COINS
from coinbase_effect.services.scrapper import get_main_top_article_topic_text

REDIS_LAST_ARTICLE_KEY = 'last_article'
REDIS_AVAILABLE_COINS_COINBASE_KEY = 'available_coins_coinbase'
REDIS_AVAILABLE_COINS_BINANCE_KEY = 'available_coins_binance'
REDIS_AVAILABLE_COINS_BINANCE_UP_LEVERAGE_KEY = 'available_coins_binance_up_leverage'
REDIS_AVAILABLE_COINS_BINANCE_DOWN_LEVERAGE_KEY = 'available_coins_binance_down_leverage'
REDIS_AVAILABLE_COINS_BINANCE_KEY_WITHOUT_COINBASE_COIN = 'available_coins_binance_without_coinbase_coin'
REDIS_COIN_RANKING_KEY_PREFIX = 'coin_ranking'
REDIS_COIN_MARKETCAP_KEY = 'coin_marketcap'
REDIS_COIN_PRICE_KEY = 'coin_price'
REDIS_LISTING_KEY = 'listing'
REDIS_BINANCE_LOT_KEY = 'binance_lot'
REDIS_BINANCE_MIN_NOTIONAL_KEY = 'binance_min_notional'

redis_connection = redis.StrictRedis(host='redis', port=6379, db=0)

logger = logging.getLogger(__name__)


class RedisStorageManager:

    def __init__(self):
        pass

    def set_data_startup(self):
        logger.info("Set startup data to redis ")
        self.set_first_article_blog()
        self.set_current_available_coins_coinbase()
        self.set_current_available_coins_binance()
        self.set_current_available_coins_binance_up_leverage()
        self.set_current_available_coins_binance_down_leverage()
        self.set_coins_on_binance_without_coinbase_coins()
        self.set_coinmarketcap_data()
        self.set_binance_coin_lots()
        self.set_binance_coin_min_notionals()

    @staticmethod
    def redis_set_dumped(key, value):
        value_dumped = json.dumps(value)
        redis_connection.set(key, value_dumped)

    @staticmethod
    def redis_get_loaded(key):
        value_dumped = redis_connection.get(key)
        return json.loads(value_dumped)

    def get_binance_up_leverage_coins(self):
        return self.redis_get_loaded(
            REDIS_AVAILABLE_COINS_BINANCE_UP_LEVERAGE_KEY
        )

    @staticmethod
    def get_price(coin_symbol_ticker):
        """We are getting price to USDT as default"""
        key = f"{REDIS_COIN_PRICE_KEY}_{coin_symbol_ticker}"
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return float(value.decode('utf-8'))
        return float(value)

    @staticmethod
    def get_ranking(coin):
        key = f"{REDIS_COIN_RANKING_KEY_PREFIX}_{coin}"
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return int(value.decode('utf-8'))
        return int(value)

    @staticmethod
    def get_market_cap(coin) -> int:
        key = f"{REDIS_COIN_MARKETCAP_KEY}_{coin}"
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return float(value.decode('utf-8'))
        return float(value)

    @staticmethod
    def is_coin_already_listed_today():
        """We avoid situation that we will buy based on both Twitter and Blog"""
        key = get_listing_today_key()
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return bool(value.decode('utf-8'))
        return None

    @staticmethod
    def get_symbol_lot(symbol):
        key = f'{REDIS_BINANCE_LOT_KEY}_{symbol}'
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return float(value.decode('utf-8'))
        return None

    @staticmethod
    def get_symbol_min_notional(symbol):
        key = f'{REDIS_BINANCE_MIN_NOTIONAL_KEY}_{symbol}'
        value = redis_connection.get(key)
        if not value:
            return None
        if isinstance(value, bytes):
            return float(value.decode('utf-8'))
        return None

    def set_current_available_coins_coinbase(self):
        self.redis_set_dumped(REDIS_AVAILABLE_COINS_COINBASE_KEY, COINBASE_AVAILABLE_COINS)

    def set_current_available_coins_binance(self):
        self.redis_set_dumped(REDIS_AVAILABLE_COINS_BINANCE_KEY, get_available_coins())

    def set_coins_on_binance_without_coinbase_coins(self):
        available_coins_coinbase = COINBASE_AVAILABLE_COINS
        available_coins_binance = self.redis_get_loaded(REDIS_AVAILABLE_COINS_BINANCE_KEY)

        coins_binance_without_coinbase = list(
            set(available_coins_binance) - set(available_coins_coinbase)
        )
        self.redis_set_dumped(
            REDIS_AVAILABLE_COINS_BINANCE_KEY_WITHOUT_COINBASE_COIN,
            coins_binance_without_coinbase
        )

    def set_current_available_coins_binance_up_leverage(self):
        self.redis_set_dumped(
            REDIS_AVAILABLE_COINS_BINANCE_UP_LEVERAGE_KEY,
            get_coins_up_levarage()
        )

    def set_current_available_coins_binance_down_leverage(self):
        self.redis_set_dumped(
            REDIS_AVAILABLE_COINS_BINANCE_DOWN_LEVERAGE_KEY,
            get_coins_down_levarage()
        )

    @staticmethod
    def set_coin_listing_today():
        """
        There can be situation that
        once info is first published on twitter
        and once first is published on blog
        so save it and don't allow to analyse second time few minutes later
        after first occurrence
        """
        key = get_listing_today_key()
        redis_connection.set(key, 1)

    @staticmethod
    def set_all_prices():
        prices = get_available_coins_price()
        for symbol, price in prices.items():
            key = f'{REDIS_COIN_PRICE_KEY}_{symbol}'
            redis_connection.set(key, price)

    @staticmethod
    def set_binance_coin_lots():
        symbol_lots = get_symbol_lots()
        for symbol, lot in symbol_lots.items():
            key = f'{REDIS_BINANCE_LOT_KEY}_{symbol}'
            redis_connection.set(key, lot)

    @staticmethod
    def set_binance_coin_min_notionals():
        min_notionals = get_symbol_min_notional()
        for symbol, min_notional in min_notionals.items():
            key = f'{REDIS_BINANCE_MIN_NOTIONAL_KEY}_{symbol}'
            redis_connection.set(key, min_notional)

    @staticmethod
    def set_first_article_blog():
        text = None
        while not text:
            try:
                text = get_main_top_article_topic_text()
            except HTTPError as err:
                logger.error(
                    'Error while trying to get top article topic text and for setting it to redis. Error: %s',
                    err
                )
                continue
        redis_connection.set(REDIS_LAST_ARTICLE_KEY, text)

    def set_coinmarketcap_data(self):
        logger.info(
            'Start settings coin market cap data'
        )
        response_data = get_listing_latest_data()
        response_data_generator = (
            item for item in response_data
        )
        while True:
            try:
                item = next(response_data_generator)
            except StopIteration:
                break

            try:
                coin_symbol = item['symbol']
                coin_ranking = item['cmc_rank']
                coin_market_cap = item['quote']['USD']['market_cap']
            except KeyError as err:
                logger.error(
                    'Key error: %s in item: %s ', err, item
                )
                continue

            self.set_coinmarketcap_ranking(
                coin_symbol=coin_symbol,
                value=coin_ranking
            )
            self.set_coinmarketcap_market_cap(
                coin_symbol=coin_symbol,
                value=coin_market_cap
            )

    @staticmethod
    def set_coinmarketcap_ranking(coin_symbol, value):
        KEY = f'{REDIS_COIN_RANKING_KEY_PREFIX}_{coin_symbol}'
        redis_connection.set(KEY, value)

    @staticmethod
    def set_coinmarketcap_market_cap(coin_symbol, value):
        KEY = f'{REDIS_COIN_MARKETCAP_KEY}_{coin_symbol}'
        redis_connection.set(KEY, value)


def get_listing_today_key():
    date_today = date.today().isoformat()
    return f'{REDIS_LISTING_KEY}_{date_today}'
