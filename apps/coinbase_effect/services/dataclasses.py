from dataclasses import dataclass

from coinbase_effect.services.potential_growth_calculator import PotentialGrowthCalculator


@dataclass
class Config:
    amount_to_invest: str


@dataclass
class TransactionCoin:

    coin_object: PotentialGrowthCalculator
    amount_to_invest: float
    transaction_buy: 'TransactionBuy' = None
    transaction_sell: 'TransactionSell' = None


@dataclass
class Transaction:
    is_success: bool
    symbol: str
    symbol_ticker: str
    amount_to_invest: float
    quantity_ask: float
    quantity_filled: float
    price_filled: float
    price_ask: float = None  # because can be market order


@dataclass
class TransactionBuy(Transaction):
    pass


@dataclass
class TransactionSell(Transaction):
    pass