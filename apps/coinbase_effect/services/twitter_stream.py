import logging
import time

import urllib3
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

from queue import Queue

# https://www.dataquest.io/blog/streaming-data-python/
# https://python-twitter.readthedocs.io/en/latest/twitter.html#module-twitter.api
from api_twitter.utils import (
    consumer_api_key,
    consumer_api_key_secret,
    access_token_secret,
    access_token,
    STREAM_IDS_TO_FOLLOW_NAME_TO_ID,
)
from coinbase_effect.services.analysers.twitter import AnalyserTwitter

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


queue_coinbase = Queue(maxsize=1000)


class Listener(StreamListener):
    """
    A listener handles tweets that are received from the stream.
    """
    def __init__(self):
        # self.queue = queue
        self.analyser_twitter = AnalyserTwitter()
        super(Listener, self).__init__()

    def on_data(self, raw_data):
        # self.queue.put(raw_data)
        self.analyser_twitter.analyse_tweet(raw_data)
        # analyse_tweet(raw_data)
        logger.debug(f"New tweet on queue: {raw_data}")

    def on_exception(self, exception):
        logger.error("exception: {}".format(exception))

    def on_limit(self, track):
        logger.info("limitation notice arrived: {}".format(track))
        return

    def on_error(self, status_code):
        logger.error("error code: {}".format(status_code))
        if status_code == 420:
            time.sleep(60)
            logger.error("Exiting system to restart container")
            raise SystemExit('Exit to restart container')

    def on_timeout(self):
        logger.warning("timeout")

    def on_disconnect(self, notice):
        logger.error("disconnected: {}".format(notice))

    def on_warning(self, notice):
        logger.warning("warning: {}".format(notice))
        return


def run():
    logger.info('RUN ! ')
    listener = Listener()
    auth = OAuthHandler(consumer_api_key, consumer_api_key_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, listener)
    logger.info(
        "starting following Twitter IDs: {}".format(
            STREAM_IDS_TO_FOLLOW_NAME_TO_ID.keys()
        )
    )
    try:
        stream.filter(follow=STREAM_IDS_TO_FOLLOW_NAME_TO_ID.values())
    except urllib3.exceptions.ProtocolError as err:
        #  try again
        logger.exception('Error while stream filter. Error: %s', err)
        pass
    logger.info('FILTERED ')
