import time
import threading
import math
import os
import logging
from typing import List, Dict, Iterable

from typing import Union
from binance.client import Client

from coinbase_effect.services.dataclasses import TransactionCoin, TransactionBuy, TransactionSell
from coinbase_effect.services.potential_growth_calculator import PotentialGrowthCalculator
from krypto_farma.settings_binance import BINANCE_API_KEY, BINANCE_SECRET_KEY
from coinbase_effect.services.utils import get_order_of_magnitude

client = Client(BINANCE_API_KEY, BINANCE_SECRET_KEY)


logger = logging.getLogger(__name__)


class ManagerTransaction:

    def __init__(self, amount_to_invest=2500):
        # TODO add from database config not raw value
        self.buy_manager = BuyManager()
        self.sell_manager = SellManager()
        self.coins_transaction = []
        self.threads = []
        self.amount_to_invest = amount_to_invest

    def perform(self, coins_calculated_sorted: List[PotentialGrowthCalculator]):
        amount_per_coin = self.get_amount_per_coin(
            amount_to_invest=self.amount_to_invest,
            coins_length=len(coins_calculated_sorted)
        )
        for coin in coins_calculated_sorted:
            transaction_coin = TransactionCoin(
                coin_object=coin,
                amount_to_invest=amount_per_coin,
            )
            thread_perform_single = threading.Thread(
                target=self.perform_single_coin,
                name=f'perform_single_{coin.symbol}',
                args=(transaction_coin, )
            )
            self.threads.append(thread_perform_single)
            thread_perform_single.start()

    def perform_single_coin(self, transaction_coin: TransactionCoin):
        transaction_coin.transaction_buy = self.buy_manager.perform_single(transaction_coin)
        transaction_coin.transaction_sell = self.sell_manager.perform_single(transaction_coin)
        self.coins_transaction.append(transaction_coin)

    def has_bought_successfully(self):
        [thread.join() for thread in self.threads]
        return any(
            [transaction_coin.transaction_buy.is_success
             for transaction_coin in self.coins_transaction]
        )

    @staticmethod
    def get_amount_per_coin(amount_to_invest, coins_length):
        return round(amount_to_invest / coins_length, 2)


class BuyManager:

    def perform_single(self, transaction_coin):
        coin = transaction_coin.coin_object
        amount_to_invest = transaction_coin.amount_to_invest
        quantity = self.get_quantity_to_buy(
            coin=coin,
            amount_to_invest=amount_to_invest
        )

        logger.info(
            'Creating buy order: '
            '\nsymbol=%s \nquantity=%s \namount_to_invest=%s \nlast_price=%s',
            coin.symbol,
            quantity,
            amount_to_invest,
            coin.price,
        )

        response = client.create_order(
            symbol=coin.symbol_ticker_usdt,
            side=client.SIDE_BUY,
            type=client.ORDER_TYPE_MARKET,
            quantity=quantity,
        )
        logger.info('Buy response dict: %s', response)

        transaction_is_success = (
            response['status'] == client.ORDER_STATUS_FILLED
        )
        transaction_symbol = response['symbol']
        transaction_quantity = response['fills'][0]['qty']
        transaction_status = response['status']
        transaction_price = response['fills'][0]['price']

        transaction_buy = TransactionBuy(
            is_success=transaction_is_success,
            symbol=coin.symbol,
            symbol_ticker=transaction_symbol,
            amount_to_invest=amount_to_invest,
            price_filled=transaction_price,
            quantity_ask=quantity,
            quantity_filled=transaction_quantity,
        )
        return transaction_buy

    @staticmethod
    def get_quantity_to_buy(coin: PotentialGrowthCalculator, amount_to_invest: float):
        symbol_lot = coin.symbol_lot
        if not coin.symbol_lot:
            symbol_lot = 1
        magnitude_lot = get_order_of_magnitude(symbol_lot)
        return round(
            float(amount_to_invest / coin.price),
            -magnitude_lot
        )


class SellManager:

    def perform_single(self, transaction: TransactionCoin):
        transaction.coin_object.set_price_sell()
        quantity_ask = self.get_quantity_to_sell(
            quantity_ask=transaction.transaction_buy.quantity_ask
        )
        price_ask = transaction.coin_object.price_sell

        logger.info(
            'Creating sell order: '
            'symbol=%s, quantity=%s, price_sell=%s, price_sell_percentage=%s',
            transaction.transaction_buy.symbol_ticker,
            quantity_ask,
            price_ask,
            transaction.coin_object.price_sell_percentage,
        )

        response = client.create_order(
            symbol=transaction.transaction_buy.symbol_ticker,
            side=client.SIDE_SELL,
            type=client.ORDER_TYPE_LIMIT,
            timeInForce=client.TIME_IN_FORCE_GTC,
            quantity=quantity_ask,
            price=price_ask,
        )
        logger.info('Sell response dict: %s', response)

        transaction_status = response['status']
        transaction_is_success = response['status'] == client.ORDER_STATUS_NEW
        transaction_price = response['price']
        transaction_quantity = response['origQty']

        transaction = TransactionSell(
            symbol=transaction.coin_object.symbol,
            symbol_ticker=transaction.coin_object.symbol_ticker_usdt,
            is_success=transaction_is_success,
            amount_to_invest=transaction.transaction_buy.amount_to_invest,
            quantity_ask=quantity_ask,
            price_ask=price_ask,
            price_filled=transaction_price,
            quantity_filled=transaction_quantity,
        )
        return transaction

    @staticmethod
    def get_quantity_to_sell(quantity_ask):
        """
        After "Buy Market Order" there can be multi responses
        so "quantity filled" is not original "quantity ask"
        so we will selling about 95% of all sum
        """
        return quantity_ask * 1