import logging
import json

import numpy as np
from sklearn.linear_model import LinearRegression

from coinbase_effect.services.redis_storage_manager import RedisStorageManager
from coinbase_effect.services.utils import get_order_of_magnitude
from coinbase_effect.services.constants import PREVIOUS_OCCURRENCES


logger = logging.getLogger(__name__)


class PotentialGrowthCalculator:

    QUOTA_SYMBOL = 'USDT'
    DETAILS_FIELDS = [
        'symbol', 'symbol_ticker_usdt', 'price', 'symbol_lot', 'ranking', 'market_cap', 'price_sell'
    ]

    def __init__(self, symbol, storage_manager):
        self.symbol = symbol
        self.storage_manager = storage_manager
        self.symbol_ticker_usdt = self.get_coin_symbol_ticker_usdt(symbol)

        self.price = self.storage_manager.get_price(self.symbol_ticker_usdt)
        self.symbol_lot = self.storage_manager.get_symbol_lot(self.symbol_ticker_usdt)
        self.min_notional = self.storage_manager.get_symbol_min_notional(self.symbol_ticker_usdt)
        self.ranking = self.storage_manager.get_ranking(symbol)
        self.market_cap = self.storage_manager.get_market_cap(symbol)
        self.price_sell = None
        self.price_sell_percentage = None

    def __str__(self):
        return self.symbol

    def set_price_sell(self):
        logger.info('Setting price')
        try:
            self.price_sell_percentage = self.get_price_sell_percentage_calculated(
                coin_symbol_ticker_usdt=self.symbol_ticker_usdt,
                ranking=self.ranking,
                market_cap=self.market_cap,
            )
        except Exception:  # IN CASE OF SOMETHING
            logger.error('Set price sell percentage went wrong, set default: 0.4')
            self.price_sell_percentage = 0.4

        try:
            self.price_sell = self.get_price_sell_calculated(
                price_before_news=self.price,
                price_sell_percentage=self.price_sell_percentage,
                min_notional=self.min_notional
            )
        except Exception:  # IN CASE OF SOMETHING
            logger.error('Set price sell went wrong, set default: self.price * (1+ 0.4')
            self.price_sell = self.price * (1 + self.price_sell_percentage)

    def get_coin_symbol_ticker_usdt(self, coin):
        binance_coins_up_leverage = self.storage_manager.get_binance_up_leverage_coins()
        coin_leverage_name = f'{coin}UP'
        if coin_leverage_name in binance_coins_up_leverage:
            return f'{coin_leverage_name}{self.QUOTA_SYMBOL}'
        return f'{coin}{self.QUOTA_SYMBOL}'

    @staticmethod
    def get_price_sell_calculated(price_before_news, price_sell_percentage, min_notional):
        magnitude_lot = get_order_of_magnitude(min_notional)
        price = price_before_news * (1 + price_sell_percentage)
        return round(price, -magnitude_lot)

    @staticmethod
    def get_price_sell_percentage_calculated(
        coin_symbol_ticker_usdt,
        ranking,
        market_cap
    ):
        multiplier = 1
        if 'UP' in coin_symbol_ticker_usdt:
            multiplier = 1.5

        all_occurrences = []
        try:
            with open('coinbase_effect/previous_growth.json') as file:
                logger.info("File was found correctly and we assign content of file to variable")
                json_results = json.load(file)
        except FileNotFoundError:
            logger.error('File was not found so we assign default')
            json_results = PREVIOUS_OCCURRENCES

        if not json_results:
            logger.error('JSON is still empty')
            json_results = PREVIOUS_OCCURRENCES

        for item in json_results:
            all_occurrences.extend(item['occurrences'])

        y = []
        x = []
        for occurrence in all_occurrences:
            y.append(occurrence['growth'])
            x.append([
                occurrence['ranking'],
                occurrence['marketcap']
            ])

        reg = LinearRegression().fit(x, y)
        reg.score(x, y)
        result = reg.predict(np.array([[ranking, market_cap,
        ]]))

        percentage = round(
            (float(result) * multiplier)/100,
            3
        )
        logger.info(
            'For entry data: ranking=%s, market_cap=%s, multiplier=%s, calculated percentage=%s',
            ranking, market_cap, multiplier, percentage
        )
        return percentage

    def get_details_dict(self):
        return {
            field: getattr(self, field, None) for field in self.DETAILS_FIELDS
        }


class PotentialGrowthCalculatorBacktest:

    def __init__(self):
        self.occurrences = self.save_occurrences_from_file()
        self.last_occurrence = self.occurrences[0]['occurrences']
        self.storage_manager = RedisStorageManager()

    @staticmethod
    def save_occurrences_from_file():
        with open('coinbase_effect/previous_growth.json') as file:
            return json.load(file)

    def get_report(self):
        report = {}
        for occurrence in self.last_occurrence:
            coin = occurrence['coin']
            price = occurrence['price']
            ranking = occurrence['ranking']
            marketcap = occurrence['marketcap']
            growth = occurrence['growth']
            potential_growth_calculator = PotentialGrowthCalculator(coin, self.storage_manager)

            price_sell_predicted, price_sell_percentage_predicted = self.get_sell_price_backtest_prediction(
                potential_growth_calculator, price, ranking, marketcap
            )

            report[coin] = {
                'price_sell_percentage_predicted': price_sell_percentage_predicted,
                'price_sell_percentage_grown': growth/100
            }
        return report

    @staticmethod
    def get_sell_price_backtest_prediction(
            potential_growth_calculator: PotentialGrowthCalculator,
            price,
            ranking,
            marketcap
    ):
        potential_growth_calculator.price = price
        potential_growth_calculator.ranking = ranking
        potential_growth_calculator.market_cap = marketcap
        potential_growth_calculator.set_price_sell()
        return potential_growth_calculator.price_sell, potential_growth_calculator.price_sell_percentage
