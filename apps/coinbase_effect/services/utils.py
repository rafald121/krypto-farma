import math


def get_order_of_magnitude(number):
    return math.floor(math.log(number, 10))


def get_coins_name_that_has_another_coin_name():
    """
    Filter that coins that in its name there is another coin name
    e.x. OG vs DOGE
    Output:
    {
        "ATOM": [
            "OM"
        ],
        "YFII": [
            "YFI"
        ],
    }
    """
    from api_binance.utils import get_available_coins
    coins = get_available_coins()
    repetitions = {}
    for c in coins:
        repetitions[c] = []
        for cc in coins:
            if cc in c and cc != c:
                repetitions[c].append(cc)

    result_dict = {r: v for r, v in repetitions.items() if v}
    result_dict_without_up_and_down = {}
    for r, v in result_dict.items():
        if 'UP' in r or 'DOWN' in r or 'BEAR' in r or 'BULL' in r:
            continue
        result_dict_without_up_and_down[r] = v
    return result_dict_without_up_and_down


def get_coins_that_are_in_other_coins(coins_name_that_has_another_coin_name):
    """
    Input:
    {
        "ATOM": [
            "OM"
        ],
        "YFII": [
            "YFI"
        ],
    }
    Output:
    {
        "OM": [
            "ATOM",
            "PROM",
            "TOMO",
            "OMG",
            "COMP",
            "LOOM"
        ],
            "YFI": [
            "YFII"
        ],
    }
    :return:
    """
    new_dict = {}
    for key, coin_list in coins_name_that_has_another_coin_name:
        for coin in coin_list:
            if coin in new_dict:
                new_dict[coin].append(key)
            else:
                new_dict[coin] = [key]
    return new_dict


def test_which_coin_will_be_first():
    coins_from_topic = set()
    for first, second in get_coins_name_that_has_another_coin_name().items():
        coins_from_topic.add(first)
        for s in second:
            coins_from_topic.add(s)

    from coinbase_effect.services.analysers.common import Analyser
    a = Analyser()
    first_coin = {}
    for c in coins_from_topic:
        first_coin[c] = a.get_coins_from_topic_text(c)

    return dict(
        sorted(first_coin.items())
    )
