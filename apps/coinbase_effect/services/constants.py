COINBASE_AVAILABLE_COINS = [
    'AAVE',
    'ALGO',
    'ATOM',
    'BAL',
    'BAND',
    'BAT',
    'BCH',
    'BNT',
    'BSV',
    'BTC',
    'CELO',
    'COMP',
    'CVC',
    'DAI',
    'DASH',
    'DNT',
    'EOS',
    'ETC',
    'ETH',
    'FIL',
    'GRT',
    'GNT',
    'KNC',
    'LINK',
    'LOOM',
    'LRC',
    'LTC',
    'MANA',
    'MATIC',
    'MKR',
    'NMR',
    'NU',
    'OMG',
    'OXT',
    'REN',
    'REP',
    'SUSHI',
    'SKL',
    'SNX',
    'USDC',
    'UMA',
    'UNI',
    'WBTC',
    'XLM',
    'XRP',
    'XTZ',
    'YFI',
    'ZEC',
    'ZRX',
    'ADA',
    'ANKR',
    'CRV',
    'STORJ',
    'ENJ',
    '1INCH',
    'NKN',
    'OGN',
    'CTSI',
    'RLC',
    'MIR',
    'TRB',
    'ICP',
]


PREVIOUS_OCCURRENCES = [
  {
    "date": "07-04-2021",
    "occurrences": [
      {
        "coin": "1INCH",
        "price": 5.07,
        "ranking": 108,
        "marketcap": 784000000,
        "growth": 17.5,
        "minutes": 6,
        "growth_predicted": 0.302
      },
      {
        "coin": "ENJ",
        "price": 2.2,
        "ranking": 62,
        "marketcap": 1943000000,
        "growth": 18.62,
        "minutes": 6,
        "growth_predicted": 0.228
      },
      {
        "coin": "NKN",
        "price": 0.577,
        "ranking": 176,
        "marketcap": 308000000,
        "growth": 49,
        "minutes": 7,
        "growth_predicted": 0.414
      },
      {
        "coin": "OGN",
        "price": 2.49,
        "ranking": 122,
        "marketcap": 645000000,
        "growth": 29,
        "minutes": 2,
        "growth_predicted": 0.325
      }
    ]
  },
  {
    "date": "23-03-2021",
    "occurrences": [
      {
        "coin": "ANKR",
        "ranking": 116,
        "marketcap": 496000000,
        "growth": 33.77
      },
      {
        "coin": "CRV",
        "ranking": 103,
        "marketcap": 600000000,
        "growth": 19
      },
      {
        "coin": "STORJ",
        "ranking": 162,
        "marketcap": 223000000,
        "growth": 31
      }
    ]
  },
  {
    "date": "16-03-2021",
    "occurrences": [
      {
        "coin": "ADA",
        "ranking": 5,
        "marketcap": 33000000000,
        "growth": 18.38
      }
    ]
  },
  {
    "date": "09-03-2021",
    "occurrences": [
      {
        "coin": "MATIC",
        "ranking": 52,
        "marketcap": 1490000000,
        "growth": 25.7
      },
      {
        "coin": "SKL",
        "ranking": 223,
        "marketcap": 317000000,
        "growth": 52.8
      },
      {
        "coin": "SUSHI",
        "ranking": 40,
        "marketcap": 2360000000,
        "growth": 23.2
      }
    ]
  },
  {
    "date": "14-12-2020",
    "occurrences": [
      {
        "coin": "BNT",
        "ranking": 98,
        "marketcap": 114000000,
        "growth": 45.22
      }
    ]
  }
]


COINS_THAT_CONTAIN_OTHER_COINS = {
  "OM": [
    "ATOM",
    "PROM",
    "TOMO",
    "OMG",
    "COMP",
    "LOOM"
  ],
  "YFI": [
    "YFII"
  ],
  "BCH": [
    "BCHSV",
    "BCHABC",
    "BCHA"
  ],
  "GO": [
    "DEGO",
    "ALGO",
    "AERGO"
  ],
  "WIN": [
    "WING",
    "WINGS"
  ],
  "WING": [
    "WINGS"
  ],
  "TUSD": [
    "TUSDB"
  ],
  "GBP": [
    "BGBP"
  ],
  "REP": [
    "DREP"
  ],
  "SC": [
    "SCRT"
  ],
  "BCHA": [
    "BCHABC"
  ],
  "AUD": [
    "AUDIO"
  ],
  "OG": [
    "DOGE",
    "OGN"
  ],
  "LUN": [
    "LUNA"
  ],
  "ZRX": [
    "BZRX"
  ],
  "AE": [
    "AERGO"
  ],
  "BTC": [
    "BTCB",
    "RENBTC",
    "WBTC",
    "BTCST"
  ],
  "REN": [
    "RENBTC"
  ],
  "AVA": [
    "KAVA",
    "AVAX"
  ],
  "ETH": [
    "BETH"
  ],
  "FUEL": [
    "TFUEL"
  ],
  "COS": [
    "COCOS"
  ],
  "USDS": [
    "USDSB"
  ],
  "VIB": [
    "VIBE"
  ],
  "ONT": [
    "FRONT"
  ],
  "OST": [
    "IOST"
  ],
  "PHA": [
    "ALPHA"
  ],
  "PAX": [
    "PAXG"
  ]
}

COINS_THAT_CONTAIN_OTHER_COINS_REVERTED = {
  "ATOM": [
    "OM"
  ],
  "YFII": [
    "YFI"
  ],
  "BCHSV": [
    "BCH"
  ],
  "DEGO": [
    "GO"
  ],
  "WING": [
    "WIN"
  ],
  "WINGS": [
    "WING",
    "WIN"
  ],
  "PROM": [
    "OM"
  ],
  "TUSDB": [
    "TUSD"
  ],
  "BGBP": [
    "GBP"
  ],
  "DREP": [
    "REP"
  ],
  "TOMO": [
    "OM"
  ],
  "ALGO": [
    "GO"
  ],
  "SCRT": [
    "SC"
  ],
  "BCHABC": [
    "BCH",
    "BCHA"
  ],
  "AUDIO": [
    "AUD"
  ],
  "DOGE": [
    "OG"
  ],
  "LUNA": [
    "LUN"
  ],
  "BZRX": [
    "ZRX"
  ],
  "AERGO": [
    "GO",
    "AE"
  ],
  "BTCB": [
    "BTC"
  ],
  "RENBTC": [
    "REN",
    "BTC"
  ],
  "OGN": [
    "OG"
  ],
  "KAVA": [
    "AVA"
  ],
  "OMG": [
    "OM"
  ],
  "WBTC": [
    "BTC"
  ],
  "BETH": [
    "ETH"
  ],
  "TFUEL": [
    "FUEL"
  ],
  "COCOS": [
    "COS"
  ],
  "USDSB": [
    "USDS"
  ],
  "BTCST": [
    "BTC"
  ],
  "VIBE": [
    "VIB"
  ],
  "BCHA": [
    "BCH"
  ],
  "FRONT": [
    "ONT"
  ],
  "COMP": [
    "OM"
  ],
  "IOST": [
    "OST"
  ],
  "ALPHA": [
    "PHA"
  ],
  "AVAX": [
    "AVA"
  ],
  "PAXG": [
    "PAX"
  ],
  "LOOM": [
    "OM"
  ]
}
