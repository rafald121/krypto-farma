import logging
import os
import smtplib
from email.message import EmailMessage


logger = logging.getLogger(__name__)


class Notifier:

    SUBJECT_REAL = 'Coinbase effect REAL notification'
    DIVIDER = '==============='

    def __init__(self):
        self.gmail_user = os.environ['EMAIL_USER']
        self.gmail_password = os.environ['EMAIL_PASSWORD']
        self.server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        self.login()

    def login(self):
        try:
            self.server.ehlo()
            self.server.login(self.gmail_user, self.gmail_password)
        except Exception as err:
            logger.error(
                'Error while login, details: %s', err
            )

    def notify(self, coins_transaction):
        email_message = EmailMessage()
        email_message['Subject'] = self.SUBJECT_REAL
        email_message['From'] = self.gmail_user
        email_message['To'] = self.gmail_user
        email_message['X-Priority'] = '1'  # priority to notify me
        email_message['X-MSMail-Priority'] = 'High'

        try:
            email_message_body = self.get_email_body(coins_transaction)
        except TypeError as err:
            logger.error(
                'Error while get email body based on data.'
            )
            email_message_body = (
                'Error while generating body. You have to check it manually'
            )

        email_message.set_content(email_message_body)
        logger.info(
            'Sending email from_addr: %s, to_addr: %s, with body: %s',
            self.gmail_user,
            self.gmail_user,
            email_message_body,
        )
        try:
            self.server.send_message(
                msg=email_message,
                from_addr=self.gmail_user,
                to_addrs=self.gmail_user
            )
            logger.info('Email sent')
            self.server.close()
        except Exception as err:
            logger.error('Error while sending email: %s', err)

    def get_email_body(self, coins_transaction):
        email_body = self.DIVIDER

        for coin in coins_transaction:
            email_body += self.get_single_coin_data(transaction_coin=coin)
            email_body += self.DIVIDER
        return email_body


    def get_single_coin_data(self, transaction_coin):
        coin_detail = f'''{self.DIVIDER}\nBUY:\n'''
        coin_detail += ''.join(
            f'{attr}:{value or "None"}\n' for attr, value in
            transaction_coin.transaction_buy.__dict__.items()
        )
        coin_detail += '\nSELL:\n'
        coin_detail += ''.join(
            f'{attr}:{value or "None"}\n' for attr, value in
            transaction_coin.transaction_sell.__dict__.items()
        )
        coin_detail += f'\n{self.DIVIDER}\n'
        return coin_detail
