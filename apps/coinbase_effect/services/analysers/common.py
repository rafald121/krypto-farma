import time
import logging
from typing import List, Union, Dict


from coinbase_effect.services.constants import COINS_THAT_CONTAIN_OTHER_COINS
from coinbase_effect.services.notifier import Notifier
from coinbase_effect.services.potential_growth_calculator import PotentialGrowthCalculator
from coinbase_effect.services.redis_storage_manager import (
    REDIS_AVAILABLE_COINS_BINANCE_KEY_WITHOUT_COINBASE_COIN,
    RedisStorageManager,
)
from coinbase_effect.services.transactions import ManagerTransaction

logger = logging.getLogger(__name__)


class Analyser:
    BLOG_TOPIC_KEYWORDS = [
        'are available', 'is available',
        'are now available', 'is now available',
        'are launching', 'is launching',
        'are now launching', 'is now launching',
        'are live', 'is live',
        'are now live', 'is now live',
        'will be available',
    ]

    def __init__(self):
        self.storage_manager = RedisStorageManager()
        self.storage_manager.set_data_startup()
        self.manager_transaction = ManagerTransaction()
        self.notifier = Notifier()

    def on_new_text(self, text: str):

        if self.storage_manager.is_coin_already_listed_today():
            logger.info('Info was already published before')
            return

        self.set_new_text_appeared()
        tweet_data_text_coins = self.get_coins_from_topic_text(text)
        if not tweet_data_text_coins:
            logger.info('Not found any coins in text: %s', text)
            return

        logger.info('Coins from text: %s', tweet_data_text_coins)
        coins_calculated = self.get_coins_potential_growth_calculated(
            tweet_data_text_coins
        )
        coins_calculated_sorted = self.get_coins_to_invest_in_proper_order(coins_calculated)
        if not coins_calculated_sorted:
            logger.error('Not found coin to invest from coins_calculated: %s', coins_calculated)
            return

        logger.info('Coins choose sorted: %s', [str(c) for c in coins_calculated_sorted])
        self.manager_transaction.perform(coins_calculated_sorted)

        if self.should_notify_by_email():
            self.notifier.notify(
                coins_transaction=self.manager_transaction.coins_transaction
            )

    def should_notify_by_email(self):
        return self.manager_transaction.has_bought_successfully()

    @staticmethod
    def generate_time_report(
        time_start,
        time_before_buy,
        time_before_notify
    ):
        logger.info(
            'time before buy: %s, time before sell: %s, time before notify: %s',
            round(time_before_buy - time_start, 4),
            round(time_before_notify - time_start, 4),
        )

    def set_new_text_appeared(self):
        self.storage_manager.set_coin_listing_today()

    @staticmethod
    def get_best_coin_to_invest(
            coins_calculated: List[PotentialGrowthCalculator]
    ) -> Union[PotentialGrowthCalculator, None]:
        coins_calculated_ranking_sorted = sorted(coins_calculated, key=lambda x: x.ranking, reverse=True)
        if coins_calculated_ranking_sorted:
            return coins_calculated_ranking_sorted[0]
        return None

    @staticmethod
    def get_coins_to_invest_in_proper_order(
        coins_calculated: List[PotentialGrowthCalculator]
    ) -> List[PotentialGrowthCalculator]:
        return [coin for coin in sorted(coins_calculated, key=lambda x: x.ranking, reverse=True)]

    def get_coins_potential_growth_calculated(self, topic_text_coins: List) -> List[PotentialGrowthCalculator]:
        coins_calculated = []
        for coin in topic_text_coins:
            logger.info('Analysing for coin: %s', coin)
            coin_calculated = PotentialGrowthCalculator(coin, self.storage_manager)
            logger.info(
                'Coin calculated details: %s',
                coin_calculated.get_details_dict()
            )
            coins_calculated.append(coin_calculated)
        return coins_calculated

    def is_new_text_about_listing(self, topic_text):
        """
        To pass condition that there are new blog post about listing,
        following conditions have to be met:
        - 'is launching' or 'are launching' is in topic text
        - there are COINS names in topic_text that are not already in Coinbase available coins list
        - there are COINS names in topic_text that are already in Binance because we only buy here
        """
        for keyword in self.BLOG_TOPIC_KEYWORDS:
            if keyword in topic_text.lower():
                return True
        return False

    def get_coins_from_topic_text(self, topic_text: str) -> List:
        """
        We assume that we can buy only on Binance
        so we check if there are some coins in "topic_text"
        that are available on Binance
        """
        start = time.time()
        coins_are_on_binance_that_are_not_coinbase = self.storage_manager.redis_get_loaded(
            REDIS_AVAILABLE_COINS_BINANCE_KEY_WITHOUT_COINBASE_COIN
        )
        coins_found = []
        for coin in coins_are_on_binance_that_are_not_coinbase:
            if coin in topic_text:
                if not self.is_coin_contain_other_coin(coin):
                    coins_found.append(coin)
                else:
                    coin_proper = self.get_proper_coin(coin, topic_text)
                    coins_found.append(coin_proper)
        logger.info('Time took: %s', time.time() - start)
        return list(set(coins_found))  # TODO chamski fix check it after uncommenting some test will fail

    @staticmethod
    def is_coin_contain_other_coin(coin):
        return coin in COINS_THAT_CONTAIN_OTHER_COINS.keys()

    @staticmethod
    def get_proper_coin(coin, topic_text):
        proper_coins_available = COINS_THAT_CONTAIN_OTHER_COINS[coin]
        for c in proper_coins_available:
            if c in topic_text:
                return c
        return coin

    def get_coins_from_topic_text_old(self, topic_text):
        """
        We assume that we can buy only on Binance
        so we check if there are some coins in "topic_text"
        that are available on Binance
        """
        coins_are_on_binance_that_are_not_coinbase = self.storage_manager.redis_get_loaded(
            REDIS_AVAILABLE_COINS_BINANCE_KEY_WITHOUT_COINBASE_COIN
        )
        coins_found = []
        for coin in coins_are_on_binance_that_are_not_coinbase:
            if coin in topic_text:
                coins_found.append(coin)
        return coins_found
