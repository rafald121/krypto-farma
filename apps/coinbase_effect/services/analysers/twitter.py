import json
import logging

import redis

from api_twitter.utils import COINBASE_PRO, COINBASE
from coinbase_effect.services.analysers.common import Analyser

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(name)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S'
)

logger = logging.getLogger(__name__)

redis_connection = redis.StrictRedis(host='redis', port=6379, db=0)


class AnalyserTwitter(Analyser):
    CHANNELS_TO_FOLLOW = [COINBASE_PRO, COINBASE]

    def analyse_tweet(self, raw_data):
        tweet_data = self.get_tweet_data(raw_data)
        tweet_data_screen_name = tweet_data['user_screen_name'].lower()
        logger.info(
            'New tweet from: %s, content text: %s',
            tweet_data_screen_name, tweet_data['text']
        )
        if tweet_data_screen_name not in self.CHANNELS_TO_FOLLOW:
            logger.info('Tweet is not from channels: %s', self.CHANNELS_TO_FOLLOW)
            return

        tweet_data_text = tweet_data['text']
        logger.info('New text: %s', tweet_data_text)

        if self.is_new_text_about_listing(tweet_data_text):
            self.on_new_text(tweet_data_text)

    @staticmethod
    def get_tweet_data(raw_tweet):
        """
        Retrieve basic info from raw_tweet content.
        :param raw_tweet: tweet json structure represented in string format
        :return: id of the user which post the tweet, user screen name of the user which post the tweet, tweet timestamp, tweet date of creation, tweet text. Return None if any error.
        """
        # CREATE DATACLASS
        json_tweet = json.loads(raw_tweet)
        try:
            tweet_id = json_tweet["id_str"]
            user_id = json_tweet["user"]["id_str"]
            user_screen_name = json_tweet["user"]["screen_name"]
            timestamp = json_tweet["timestamp_ms"]
            created_at = json_tweet["created_at"]
            if 'extended_tweet' in json_tweet:
                extended_tweet = json_tweet.get('extended_tweet')
                text = extended_tweet.get('full_text')
            else:
                text = json_tweet.get("text")
        except KeyError as e:
            logging.error("KeyError Exception: {}. Tweet json: {}".format(e, json_tweet))
            return None
        return {
            'user_id': user_id,
            'user_screen_name': user_screen_name,
            'tweet_id': tweet_id,
            'timestamp': timestamp,
            'created_at': created_at,
            'text': text
        }

    def set_new_text_appeared(self):
        logger.info('Set new text listing on Twitter')
        super().set_new_text_appeared()
