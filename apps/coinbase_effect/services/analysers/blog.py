import logging
import time

import redis

from coinbase_effect.services.analysers.common import Analyser
from coinbase_effect.services.redis_storage_manager import (
    REDIS_LAST_ARTICLE_KEY,
)

from coinbase_effect.services.scrapper import get_main_top_article_topic_text

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(name)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S'
)

logger = logging.getLogger(__name__)

redis_connection = redis.StrictRedis(host='redis', port=6379, db=0)


class AnalyserBlog(Analyser):

    SLEEP_SECONDS = 1
    SLEEP_ON_ERROR_SECONDS = 15

    def __init__(self):
        self.request_count = 0
        super(AnalyserBlog, self).__init__()

    def main_loop(self):
        while True:
            logger.debug(
                'Requesting about top article. Request count: %s', self.request_count
            )
            self.request_count += 1

            topic_text = get_main_top_article_topic_text()

            if not topic_text:
                continue

            if (
                self.is_new_text_on_page(topic_text)
                and self.is_new_text_about_listing(topic_text)
            ):
                logger.info(
                    'New text on page and it is about listing after requests: %s. Text: %s',
                    self.request_count, topic_text
                )
                self.on_new_text(topic_text)
            else:
                logger.debug("Text is not new or is not about listing, text: %s", topic_text)
            time.sleep(self.SLEEP_SECONDS)

    def set_new_text_appeared(self):
        logger.info('Set new text listing on Blog')
        super().set_new_text_appeared()

    @staticmethod
    def is_new_text_on_page(text_fetched):
        redis_last_saved_article = redis_connection.get(REDIS_LAST_ARTICLE_KEY)
        return text_fetched != redis_last_saved_article.decode('utf-8')
