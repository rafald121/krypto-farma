import http
import logging
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from urllib.error import HTTPError


COINBASE_BLOG_URL = 'https://blog.coinbase.com/'
MAIN_BLOG_POST_TAG = 'u-fontSize30'


logger = logging.getLogger(__name__)


def get_main_top_article_topic_text():
    req = Request(COINBASE_BLOG_URL, headers={'User-Agent': 'Mozilla/5.0'})

    try:
        webpage = urlopen(req).read()
    except HTTPError as err:
        if err.code >= 500:
            logger.error('Service unavailable. Error code == %s. Error details: %s', err.code, err)

        logger.error('Service error. Error code == %s. Error details: %s', err.code, err)
        return None
    except http.client.IncompleteRead as err:
        logger.error('Incomplete Read error: %s', err)
        return None
    except Exception as err:
        logger.error('Unknown exception: %s', err)
        return None

    response_parsed = BeautifulSoup(webpage, 'html.parser')
    response_parsed_main_article = response_parsed.find_all(attrs={"class": MAIN_BLOG_POST_TAG})
    if (
        response_parsed_main_article
        and response_parsed_main_article[0]
        and hasattr(response_parsed_main_article[0], 'text')
    ):
        return response_parsed_main_article[0].text
    return None
