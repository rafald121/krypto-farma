from django.apps import AppConfig


class CoinbaseEffectConfig(AppConfig):
    name = 'coinbase_effect'
