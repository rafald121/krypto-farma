from unittest import TestCase, mock

from coinbase_effect.services.analysers.common import Analyser
from coinbase_effect.services.notifier import Notifier
from coinbase_effect.services.transactions import ManagerTransaction, SellManager
from coinbase_effect.tests.fixtures import BUY_RESPONSE_EXAMPLE, SELL_RESPONSE_EXAMPLE, CREATE_SELL_ORDER_RESPONSE_LSK, \
    CREATE_BUY_ORDER_RESPONSE_LSK, CREATE_BUY_ORDER_RESPONSE_XEM, CREATE_BUY_ORDER_RESPONSE_DOGE, \
    CREATE_SELL_ORDER_RESPONSE_XEM, CREATE_SELL_ORDER_RESPONSE_DOGE


class NotifierTestCase(TestCase):

    def setUp(self) -> None:
        patcher = mock.patch(
            'coinbase_effect.services.redis_storage_manager.RedisStorageManager.set_coinmarketcap_data'
        )
        patcher.start()
        self.manager_transaction = ManagerTransaction()
        self.coinbase_effect_analyser = Analyser()
        self.notifier = Notifier()

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('binance.client.Client.create_order')
    @mock.patch('smtplib.SMTP.send_message')
    def test_create_sell_order_single__multiple_coins__success(
        self,
        mock_send_message,
        mock_create_order,
        mock_get_market_cap,
        mock_get_ranking,
        mock_get_price
    ):
        mock_get_market_cap.side_effect = [500000, 600000, 800000]
        mock_get_ranking.side_effect = [90, 50, 5]
        mock_get_price.side_effect = [8.1, 0.32, 0.5]
        sell_response_data = [
            CREATE_SELL_ORDER_RESPONSE_LSK,
            CREATE_SELL_ORDER_RESPONSE_XEM,
            CREATE_SELL_ORDER_RESPONSE_DOGE
        ]
        mock_create_order_expected_response = [
            CREATE_BUY_ORDER_RESPONSE_LSK,
            CREATE_BUY_ORDER_RESPONSE_XEM,
            CREATE_BUY_ORDER_RESPONSE_DOGE,
            *sell_response_data
        ]
        mock_create_order.side_effect = mock_create_order_expected_response

        topic_text_coins = ['LSK', 'XEM', 'DOGE']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )
        coins_to_invest = self.coinbase_effect_analyser.get_coins_to_invest_in_proper_order(
            coins_calculated
        )

        self.manager_transaction.perform(coins_to_invest)
        self.notifier.notify(self.manager_transaction.coins_transaction)
        self.assertEqual(mock_send_message.call_count, 1)
