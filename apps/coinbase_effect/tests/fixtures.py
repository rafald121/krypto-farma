CREATE_BUY_ORDER_RESPONSE_LSK = {
    'symbol': 'LSKUSDT',
    'orderId': 1728908848,
    'orderListId': -1,
    'clientOrderId': 'KPFddS7VzLo6THEq5382ya',
    'transactTime': 1616243546732,
    'price': '0.00000000',
    'origQty': '0.06000000',
    'executedQty': '0.06000000',
    'cummulativeQuoteQty': '16.07400600',
    'status': 'FILLED',
    'timeInForce': 'GTC',
    'type': 'MARKET',
    'side': 'BUY',
    'fills': [
        {
            'price': '9.1',
            'qty': '20',
            'commission': '0.00004500',
            'commissionAsset': 'BNB',
            'tradeId': 183211209
        }
    ]
}


CREATE_BUY_ORDER_RESPONSE_XEM = {
    'symbol': 'XEMUSDT',
    'orderId': 1728908848,
    'orderListId': -1,
    'clientOrderId': 'KPFddS7VzLo6THEq5382ya',
    'transactTime': 1616243546732,
    'price': '0.00000000',
    'origQty': '3000',
    'executedQty': '3000',
    'cummulativeQuoteQty': '16.07400600',
    'status': 'FILLED',
    'timeInForce': 'GTC',
    'type': 'MARKET',
    'side': 'BUY',
    'fills': [
        {
            'price': '0.4',
            'qty': '3000',
            'commission': '0.00004500',
            'commissionAsset': 'BNB',
            'tradeId': 183211209
        }
    ]
}

CREATE_BUY_ORDER_RESPONSE_DOGE = {
    'symbol': 'DOGEUSDT',
    'orderId': 1728908849,
    'orderListId': -1,
    'clientOrderId': 'KPFddS7VzLo6THEq5382ya',
    'transactTime': 1616243546732,
    'price': '0.00000000',
    'origQty': '100',
    'executedQty': '100',
    'cummulativeQuoteQty': '16.07400600',
    'status': 'FILLED',
    'timeInForce': 'GTC',
    'type': 'MARKET',
    'side': 'BUY',
    'fills': [
        {
            'price': '0.65',
            'qty': '100',
            'commission': '0.00004500',
            'commissionAsset': 'BNB',
            'tradeId': 183211209
        }
    ]
}


CREATE_BUY_ORDER_RESPONSE_VET = {
    'symbol': 'VETUSDT',
    'orderId': 1728908810,
    'orderListId': -1,
    'clientOrderId': 'K22ddS7VzLo6THEq5382ya',
    'transactTime': 1616243546732,
    'price': '0.00000000',
    'origQty': '5',
    'executedQty': '5',
    'cummulativeQuoteQty': '16.07400600',
    'status': 'FILLED',
    'timeInForce': 'GTC',
    'type': 'MARKET',
    'side': 'BUY',
    'fills': [
        {
            'price': '0.25',
            'qty': '5',
            'commission': '0.00004500',
            'commissionAsset': 'BNB',
            'tradeId': 183211209
        }
    ]
}


CREATE_SELL_ORDER_RESPONSE_LSK = {
   "symbol":"LSKUSDT",
   "orderId":1728927698,
   "orderListId":-1,
   "clientOrderId":"tKFi8BS5llxx1cNwSSskJm",
   "transactTime":1616243979601,
   "price":"9.5",
   "origQty":"6",
   "executedQty":"0.00000000",
   "cummulativeQuoteQty":"0.00000000",
   "status":"NEW",
   "timeInForce":"GTC",
   "type":"LIMIT",
   "side":"SELL",
   "fills":[
   ]
}

CREATE_SELL_ORDER_RESPONSE_XEM = {
   "symbol":"XEMUSDT",
   "orderId":1728927698,
   "orderListId":-1,
   "clientOrderId":"tKFi8BS5llxx1cNwSSskJm",
   "transactTime":1616243979601,
   "price":"0.45",
   "origQty":"5",
   "executedQty":"0.00000000",
   "cummulativeQuoteQty":"0.00000000",
   "status":"NEW",
   "timeInForce":"GTC",
   "type":"LIMIT",
   "side":"SELL",
   "fills":[
   ]
}

CREATE_SELL_ORDER_RESPONSE_DOGE = {
   "symbol":"DOGEUSDT",
   "orderId":1728927698,
   "orderListId":-1,
   "clientOrderId":"tKFi8BS5llxx1cNwSSskJm",
   "transactTime":1616243979601,
   "price":"0.7",
   "origQty":"50",
   "executedQty":"0.00000000",
   "cummulativeQuoteQty":"0.00000000",
   "status":"NEW",
   "timeInForce":"GTC",
   "type":"LIMIT",
   "side":"SELL",
   "fills":[
   ]
}


BUY_RESPONSE_EXAMPLE = {
    'symbol': 'BNBUSDT',
    'orderId': 1728908848,
    'orderListId': -1,
    'clientOrderId': 'KPFddS7VzLo6THEq5382ya',
    'transactTime': 1616243546732,
    'price': '0.00000000',
    'origQty': '0.06000000',
    'executedQty': '0.06000000',
    'cummulativeQuoteQty': '16.07400600',
    'status': 'FILLED',
    'timeInForce': 'GTC',
    'type': 'MARKET',
    'side': 'BUY',
    'fills': [
        {
            'price': '267.90010000',
            'qty': '0.06000000',
            'commission': '0.00004500',
            'commissionAsset': 'BNB',
            'tradeId': 183211209
        }
    ]
}


SELL_RESPONSE_EXAMPLE = {
   "symbol":"BNBUSDT",
   "orderId":1728927698,
   "orderListId":-1,
   "clientOrderId":"tKFi8BS5llxx1cNwSSskJm",
   "transactTime":1616243979601,
   "price":"300.00000000",
   "origQty":"0.05000000",
   "executedQty":"0.00000000",
   "cummulativeQuoteQty":"0.00000000",
   "status":"NEW",
   "timeInForce":"GTC",
   "type":"LIMIT",
   "side":"SELL",
   "fills":[
   ]
}