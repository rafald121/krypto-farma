from unittest import mock

from django.test import TestCase
from coinbase_effect.services.analysers.blog import AnalyserBlog
from coinbase_effect.services.analysers.common import Analyser
from coinbase_effect.tests.fixtures import (
    CREATE_BUY_ORDER_RESPONSE_LSK,
    CREATE_BUY_ORDER_RESPONSE_XEM,
    CREATE_BUY_ORDER_RESPONSE_DOGE,
)


class AnalyserTestCase(TestCase):

    def setUp(self) -> None:
        patcher = mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.set_coinmarketcap_data')
        patcher.start()
        self.coinbase_effect_analyser = Analyser()


class TextAnalyserTestCase(AnalyserTestCase):

    @mock.patch('coinbase_effect.services.scrapper.get_main_top_article_topic_text')
    def test_is_new_text_about_listing(self, get_main_top_article_topic_text_mock):
        current_topic_text = 'Cardano (ADA) is launching on Coinbase'

        get_main_top_article_topic_text_mock.return_value = current_topic_text

        new_topic_text = 'VeChain (VET) is launching on Coinbase'

        is_new_text_about_listing = self.coinbase_effect_analyser.is_new_text_about_listing(new_topic_text)

        self.assertTrue(is_new_text_about_listing)

    def test_get_coins_from_topic_text(self):
        topic_text = 'VeChain (VET) is launching on Coinbase'

        topic_text_coins = self.coinbase_effect_analyser.get_coins_from_topic_text(topic_text)

        self.assertEqual(topic_text_coins, ['VET'])

    def test_get_coins_from_topic_text__few_coins__already_on_coinbase(self):
        topic_text = 'Polygon (MATIC), SKALE (SKL) and SushiSwap (SUSHI) are launching on Coinbase Pro'

        topic_text_coins = self.coinbase_effect_analyser.get_coins_from_topic_text(topic_text)

        self.assertEqual(topic_text_coins, [])  # are already on coinbase

    def test_get_coins_from_topic_text__few_coins__not_already_on_coinbase(self):
        topic_text = 'VeChain (VET), VITE (VITE) and Reef (REEF) are launching on Coinbase Pro'

        topic_text_coins = self.coinbase_effect_analyser.get_coins_from_topic_text(topic_text)

        self.assertSetEqual(set(topic_text_coins), set(['VET', 'VITE', 'REEF']))

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    def test_potential_growth_calculator(self, mock_get_market_cap, mock_get_ranking, mock_get_price):
        mock_get_market_cap.return_value = 15000
        mock_get_ranking.return_value = 50
        mock_get_price.return_value = 20

        topic_text_coins = ['VET', 'VITE', 'REEF']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )
        for coin in coins_calculated:
            self.assertEqual(coin.price, 20)
            self.assertEqual(coin.ranking, 50)
            self.assertEqual(coin.market_cap, 15000)

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    def test_potential_growth_calculator__different_values(self, mock_get_market_cap, mock_get_ranking, mock_get_price):
        mock_get_market_cap_side_effect = [15000, 25000, 35000]
        mock_get_market_cap.side_effect = mock_get_market_cap_side_effect
        mock_get_ranking_side_effect = [50, 60, 70]
        mock_get_ranking.side_effect = mock_get_ranking_side_effect
        mock_get_price_side_effect = [20, 30, 40]
        mock_get_price.side_effect = mock_get_price_side_effect

        topic_text_coins = ['VET', 'VITE', 'REEF']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )

        for i, coin in enumerate(coins_calculated):
            self.assertEqual(coin.price, mock_get_price_side_effect[i])
            self.assertEqual(coin.ranking, mock_get_ranking_side_effect[i])
            self.assertEqual(coin.market_cap, mock_get_market_cap_side_effect[i])

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    def test_get_best_coin_to_invest(self, mock_get_market_cap, mock_get_ranking, mock_get_price):
        fixture_VET = {
            'coin': 'VET',
            'market_cap': 15000,
            'ranking': 50,
            'price': 20
        }
        fixture_VITE = {
            'coin': 'VITE',
            'market_cap': 25000,
            'ranking': 60,
            'price': 30
        }
        fixture_REEF = {
            'coin': 'REEF',
            'market_cap': 35000,
            'ranking': 70,
            'price': 40
        }

        mock_get_market_cap_side_effect = [
            fixture_VET['price'],
            fixture_VITE['price'],
            fixture_REEF['price'],
        ]
        mock_get_market_cap.side_effect = mock_get_market_cap_side_effect
        mock_get_ranking_side_effect = [
            fixture_VET['ranking'],
            fixture_VITE['ranking'],
            fixture_REEF['ranking'],
        ]
        mock_get_ranking.side_effect = mock_get_ranking_side_effect
        mock_get_price_side_effect = [
            fixture_VET['price'],
            fixture_VITE['price'],
            fixture_REEF['price'],
        ]
        mock_get_price.side_effect = mock_get_price_side_effect

        topic_text_coins = [
            fixture_VET['coin'],
            fixture_VITE['coin'],
            fixture_REEF['coin'],
        ]
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )

        # choose with highest ranking
        coin_to_invest = self.coinbase_effect_analyser.get_best_coin_to_invest(coins_calculated)
        self.assertEqual(coin_to_invest.symbol, fixture_REEF['coin'])
        self.assertEqual(coin_to_invest.price, fixture_REEF['price'])


class CommonAnalyserOnNewTextTestCase(AnalyserTestCase):

    @mock.patch('smtplib.SMTP.send_message')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.is_coin_already_listed_today')
    @mock.patch('binance.client.Client.create_order')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price', return_value=10)
    @mock.patch('coinbase_effect.services.transactions.ManagerTransaction.perform_sell')
    def test_on_new_text__single_coin(
        self,
        mock_sell_manager,
        mock_get_price,
        mock_create_order,
        mock_is_already_listed,
        mock_send_email
    ):
        mock_create_order.return_value = CREATE_BUY_ORDER_RESPONSE_LSK
        mock_is_already_listed.return_value = False
        text = 'LSK is now available'
        self.coinbase_effect_analyser.on_new_text(text=text)
        mock_create_order.assert_called_once()
        mock_sell_manager.assert_called_once()

    @mock.patch('smtplib.SMTP.send_message')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.is_coin_already_listed_today')
    @mock.patch('binance.client.Client.create_order')
    @mock.patch('coinbase_effect.services.transactions.ManagerTransaction.perform_sell')
    def test_on_new_text__multiple_coins__that_are_not_in_binance(
        self,
        mock_sell,
        mock_create_order,
        mock_is_already_listed,
        mock_send_message
    ):
        # TODO fix this ...
        self.coinbase_effect_analyser.storage_manager.set_all_prices()
        self.coinbase_effect_analyser.storage_manager.set_coinmarketcap_ranking('LSK', 90)
        self.coinbase_effect_analyser.storage_manager.set_coinmarketcap_ranking('XEM', 30)
        self.coinbase_effect_analyser.storage_manager.set_coinmarketcap_ranking('DOGE', 5)
        mock_is_already_listed.return_value = False
        mock_create_order.side_effect = [
            CREATE_BUY_ORDER_RESPONSE_LSK,
            CREATE_BUY_ORDER_RESPONSE_XEM,
            CREATE_BUY_ORDER_RESPONSE_DOGE,
        ]
        crypto1 = 'LSK'
        crypto2 = 'XEM'
        crypto3 = 'DOGE'
        text = f'{crypto1} {crypto2} {crypto3} is now available'
        self.coinbase_effect_analyser.on_new_text(
            text=text,
        )
        create_order_called_with_symbol = mock_create_order.call_args_list[0][1]['symbol']
        self.assertEqual(create_order_called_with_symbol, 'LSKUSDT')
        mock_sell.assert_called_once()


class GetTopicTextCoin(AnalyserTestCase):

    def test_get_coins_from_topic_text__coin_contain_other_coin_1(self):
        crypto1 = 'LSK'
        crypto2 = 'XVG'
        crypto3 = 'DOGE'
        list_crypto = [crypto1, crypto2, crypto3]
        text_imitation = f'{crypto1} {crypto2} {crypto3} is now available'
        coins_choosed = self.coinbase_effect_analyser.get_coins_from_topic_text(text_imitation)
        self.assertSetEqual(
            set(list_crypto),
            set(coins_choosed)
        )

    def test_get_coins_from_topic_text__coin_contain_other_coin_2(self):
        crypto1 = 'TOMO'
        crypto2 = 'ALGO'
        crypto3 = 'SC'
        crypto4 = 'KAVA'
        list_crypto = [crypto1, crypto2, crypto3, crypto4]
        text_imitation = f'{crypto1} {crypto2} {crypto3} {crypto4} is now available'
        coins_choosed = self.coinbase_effect_analyser.get_coins_from_topic_text(text_imitation)
        self.assertSetEqual(
            set(list_crypto),
            set(coins_choosed)
        )

    def test_get_coins_from_topic_text__coin_contain_other_coin_3(self):
        crypto1 = 'OMG'
        crypto2 = 'GO'
        crypto3 = 'AVA'
        list_crypto = [crypto1, crypto2, crypto3, ]
        text_imitation = f'{crypto1} {crypto2} {crypto3} is now available'
        coins_choosed = self.coinbase_effect_analyser.get_coins_from_topic_text(text_imitation)
        self.assertSetEqual(
            set(list_crypto),
            set(coins_choosed)
        )

    def test_on_new_text__multiple_coins__some_are_already_on_coinbase(self):
        crypto1 = 'LSK'
        crypto2 = 'XVG'
        crypto3 = 'DOGE'
        crypto4 = '1INCH'
        list_crypto_expected = {crypto1, crypto2, crypto3}
        text_imitation = f'{crypto1} {crypto2} {crypto3} {crypto4} is now available'
        coins_choosed = self.coinbase_effect_analyser.get_coins_from_topic_text(text_imitation)
        self.assertSetEqual(
            set(list_crypto_expected),
            set(coins_choosed)
        )


class BlogAnalyserTestCase(TestCase):

    def setUp(self) -> None:
        patcher = mock.patch(
            'coinbase_effect.services.redis_storage_manager.RedisStorageManager.set_coinmarketcap_data'
        )
        patcher.start()
        self.coinbase_effect_blog_analyser = AnalyserBlog()

    @mock.patch('coinbase_effect.services.scrapper.get_main_top_article_topic_text')
    def test_is_new_text_on_page(self, get_main_top_article_topic_text_mock):
        current_topic_text = 'Cardano (ADA) is launching on Coinbase'

        get_main_top_article_topic_text_mock.return_value = current_topic_text

        new_topic_text = 'VeChain (VET) is launching on Coinbase'

        is_new_text_on_page = self.coinbase_effect_blog_analyser.is_new_text_on_page(new_topic_text)

        self.assertTrue(is_new_text_on_page)
