from unittest import TestCase, mock

from coinbase_effect.services.analysers.common import Analyser
from coinbase_effect.services.dataclasses import TransactionCoin
from coinbase_effect.services.potential_growth_calculator import PotentialGrowthCalculator
from coinbase_effect.services.transactions import ManagerTransaction, BuyManager, SellManager
from coinbase_effect.tests.fixtures import (
    CREATE_BUY_ORDER_RESPONSE_VET,
    CREATE_BUY_ORDER_RESPONSE_LSK,
    CREATE_BUY_ORDER_RESPONSE_XEM,
    CREATE_BUY_ORDER_RESPONSE_DOGE,
    CREATE_SELL_ORDER_RESPONSE_LSK,
    CREATE_SELL_ORDER_RESPONSE_XEM,
    CREATE_SELL_ORDER_RESPONSE_DOGE
)


class TransactionManagerTestCase(TestCase):

    def setUp(self) -> None:
        patcher = mock.patch(
            'coinbase_effect.services.redis_storage_manager.RedisStorageManager.set_coinmarketcap_data'
        )
        patcher.start()
        self.manager_transaction = ManagerTransaction(amount_to_invest=10.1)
        self.coinbase_effect_analyser = Analyser()

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('binance.client.Client.create_order')
    def test_buy_order__perform_single(
            self,
            mock_create_order,
            mock_get_market_cap,
            mock_get_ranking,
            mock_get_price
        ):
        mock_get_market_cap.return_value = 15000
        mock_get_ranking.return_value = 100
        coin_price = 30
        amount_to_invest = 1000
        mock_get_price.return_value = coin_price
        mock_create_order.return_value = CREATE_BUY_ORDER_RESPONSE_VET

        topic_text_coins = ['VET']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )

        quantity_expected = BuyManager.get_quantity_to_buy(
            coins_calculated[0],
            amount_to_invest=amount_to_invest
        )
        coin = coins_calculated[0]
        transaction_coin = TransactionCoin(
            coin_object=coin,
            amount_to_invest=amount_to_invest
        )
        result = self.manager_transaction.buy_manager.perform_single(transaction_coin)
        self.assertEqual(
            mock_create_order.call_args_list[0][1]['quantity'],
            quantity_expected,
        )
        self.assertEqual(
            mock_create_order.call_args_list[0][1]['symbol'],
            coin.symbol_ticker_usdt,
        )
        self.assertEqual(
            mock_create_order.call_args_list[0][1]['side'],
            'BUY',
        )
        self.assertEqual(
            mock_create_order.call_args_list[0][1]['type'],
            'MARKET',
        )
        self.assertTrue(result.symbol_ticker, coin.symbol_ticker_usdt)
        self.assertTrue(result.is_success)
        self.assertTrue(result.quantity_ask, quantity_expected)
        self.assertTrue(result.price_filled, CREATE_BUY_ORDER_RESPONSE_VET['fills'][0]['price'])
        self.assertTrue(result.quantity_filled, CREATE_BUY_ORDER_RESPONSE_VET['fills'][0]['qty'])

    # @skip
    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    # def test_create_buy_order_single_coin__start_threads_called(
    #     self,
    #     mock_get_market_cap,
    #     mock_get_ranking,
    #     mock_get_price
    # ):
    #     """Test without buy order mock"""
    #     mock_get_market_cap.return_value = 15000
    #     mock_get_ranking.return_value = 100
    #     mock_get_price.return_value = 0.15
    #     topic_text_coins = ['VET']
    #     coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
    #         topic_text_coins
    #     )
    #     self.manager_transaction.perform(coins_calculated)
    #     self.assertTrue(self.manager_transaction.has_bought_successfully())
    #     self.assertEqual(
    #         len(self.manager_transaction.coins_transaction),
    #         1
    #     )
    #     self.assertIsNotNone(
    #         self.manager_transaction.coins_transaction[0].transaction_buy
    #     )
    #     self.assertIsNotNone(
    #         self.manager_transaction.coins_transaction[0].transaction_sell
    #     )

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('coinbase_effect.services.transactions.threading.Thread.start')
    def test_create_buy_order__single_coin__start_thread_called(
        self,
        mock_thread_start,
        mock_get_market_cap,
        mock_get_ranking,
        mock_get_price
    ):
        mock_get_market_cap.return_value = 15000
        mock_get_ranking.return_value = 100
        mock_get_price.return_value = 0.15
        topic_text_coins = ['VET']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )

        self.manager_transaction.perform(coins_calculated)
        self.assertEqual(
            len(self.manager_transaction.threads),
            1
        )
        self.assertEqual(
            len(self.manager_transaction.threads[0].name),
            f'perform_single_{coins_calculated[0].symbol}'
        )

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('coinbase_effect.services.transactions.threading.Thread.start')
    def test_create_buy_order__multiple_coins__start_thread_called(
        self,
        mock_thread_start,
        mock_get_market_cap,
        mock_get_ranking,
        mock_get_price
    ):
        mock_get_market_cap.side_effect = [500000, 600000, 800000]
        mock_get_ranking.side_effect = [90, 50, 5]
        mock_get_price.side_effect = [8.1, 0.32, 0.5]

        topic_text_coins = ['LSK', 'XEM', 'DOGE']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )
        coins_to_invest = self.coinbase_effect_analyser.get_coins_to_invest_in_proper_order(
            coins_calculated
        )
        amount_to_invest = self.manager_transaction.amount_to_invest

        self.manager_transaction.perform(coins_calculated)
        self.assertEqual(len(self.manager_transaction.threads), 3)
        for i, coin in enumerate(coins_to_invest):
            self.assertEqual(
                self.manager_transaction.threads[i].name,
                f'perform_single_{coin.symbol}'
            )

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('binance.client.Client.create_order')
    @mock.patch('coinbase_effect.services.transactions.SellManager.perform_single')
    def test_create_buy_order__multiple_coins__perform_single(
        self,
        mock_perform_single,
        mock_create_order,
        mock_get_market_cap,
        mock_get_ranking,
        mock_get_price
    ):
        mock_get_market_cap.side_effect = [500000, 600000, 800000]
        mock_get_ranking.side_effect = [90, 50, 5]
        mock_get_price.side_effect = [8.1, 0.32, 0.5]
        mock_create_order_expected_response = [
            CREATE_BUY_ORDER_RESPONSE_LSK,
            CREATE_BUY_ORDER_RESPONSE_XEM,
            CREATE_BUY_ORDER_RESPONSE_DOGE,
        ]
        mock_create_order.side_effect = mock_create_order_expected_response

        topic_text_coins = ['LSK', 'XEM', 'DOGE']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )
        coins_to_invest = self.coinbase_effect_analyser.get_coins_to_invest_in_proper_order(
            coins_calculated
        )
        amount_to_invest = self.manager_transaction.amount_to_invest
        amount_per_coin_expected = self.manager_transaction.get_amount_per_coin(
            amount_to_invest, len(coins_calculated)
        )

        for coin in coins_to_invest:
            transaction_coin = TransactionCoin(
                coin_object=coin,
                amount_to_invest=amount_per_coin_expected,
            )
            self.manager_transaction.perform_single_coin(transaction_coin)

        self.assertTrue(mock_create_order.call_count, 3)

        for i, coin in enumerate(coins_calculated):
            quantity_to_buy_expected = BuyManager.get_quantity_to_buy(
                coin, amount_per_coin_expected
            )

            self.assertEqual(
                mock_create_order.call_args_list[i][1]['symbol'], coin.symbol_ticker_usdt
            )
            self.assertEqual(
                mock_create_order.call_args_list[i][1]['side'], 'BUY'
            )
            self.assertEqual(
                mock_create_order.call_args_list[i][1]['type'], 'MARKET'
            )
            self.assertEqual(
                mock_create_order.call_args_list[i][1]['quantity'],
                quantity_to_buy_expected
            )
            # TRANSACTIONS OBJECT
            self.assertEqual(
                self.manager_transaction.coins_transaction[i].coin_object,
                coins_calculated[i]
            )
            transaction_buy = self.manager_transaction.coins_transaction[i].transaction_buy
            transaction_coin_object = self.manager_transaction.coins_transaction[i].coin_object
            self.assertEqual(coin, transaction_coin_object)
            self.assertEqual(
                transaction_buy.amount_to_invest,
                amount_per_coin_expected
            )
            self.assertTrue(transaction_buy.is_success)
            self.assertEqual(
                transaction_buy.price_filled,
                mock_create_order_expected_response[i]['fills'][0]['price']
            )
            self.assertEqual(
                transaction_buy.quantity_filled,
                mock_create_order_expected_response[i]['fills'][0]['qty']
            )
            self.assertEqual(
                transaction_buy.quantity_ask,
                quantity_to_buy_expected,
            )
            self.assertEqual(
                transaction_buy.symbol_ticker,
                mock_create_order_expected_response[i]['symbol']
            )

    # TODO
    def test_create_buy_order__one_of_it_is_up_leverage(self):
        pass

    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    @mock.patch('binance.client.Client.create_order')
    def test_create_sell_order_single__multiple_coins__sell_price__set(
        self,
        mock_create_order,
        mock_get_market_cap,
        mock_get_ranking,
        mock_get_price
    ):
        mock_get_market_cap.side_effect = [500000, 600000, 800000]
        mock_get_ranking.side_effect = [90, 50, 5]
        mock_get_price.side_effect = [8.1, 0.32, 0.5]

        topic_text_coins = ['LSK', 'XEM', 'DOGE']
        coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
            topic_text_coins
        )
        coins_to_invest = self.coinbase_effect_analyser.get_coins_to_invest_in_proper_order(
            coins_calculated
        )
        amount_to_invest = self.manager_transaction.amount_to_invest
        amount_per_coin_expected = self.manager_transaction.get_amount_per_coin(
            amount_to_invest, len(coins_calculated)
        )

        for coin in coins_to_invest:
            transaction_coin = TransactionCoin(
                coin_object=coin,
                amount_to_invest=amount_per_coin_expected,
            )
            self.manager_transaction.perform_single_coin(transaction_coin)

        self.assertTrue(mock_create_order.call_count, 6)

        for i, transaction in enumerate(self.manager_transaction.coins_transaction):
            self.assertEqual(coins_calculated[i], transaction.coin_object)

            coin_object = transaction.coin_object
            price_sell_percentage_expected = PotentialGrowthCalculator.get_price_sell_percentage_calculated(
                coin_symbol_ticker_usdt=coin_object.symbol_ticker_usdt,
                ranking=coin_object.ranking,
                market_cap=coin_object.market_cap
            )
            price_sell_expected = PotentialGrowthCalculator.get_price_sell_calculated(
                price_before_news=coin_object.price,
                price_sell_percentage=price_sell_percentage_expected,
                min_notional=coin_object.min_notional
            )
            self.assertEqual(
                coin_object.price_sell_percentage,
                price_sell_percentage_expected
            )
            self.assertEqual(
                coin_object.price_sell,
                price_sell_expected
            )

    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_price')
    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_ranking')
    # @mock.patch('coinbase_effect.services.redis_storage_manager.RedisStorageManager.get_market_cap')
    # @mock.patch('binance.client.Client.create_order')
    # def test_create_sell_order_single__multiple_coins__success(
    #     self,
    #     mock_create_order,
    #     mock_get_market_cap,
    #     mock_get_ranking,
    #     mock_get_price
    # ):
        # """
        # cos sie psuje kolejność side_effect i w buy wskakuje sell
        # """
        # mock_get_market_cap.side_effect = [500000, 600000, 800000]
        # mock_get_ranking.side_effect = [90, 50, 5]
        # mock_get_price.side_effect = [8.1, 0.32, 0.5]
        # sell_response_data = [
        #     CREATE_SELL_ORDER_RESPONSE_LSK,
        #     CREATE_SELL_ORDER_RESPONSE_DOGE,
        #     CREATE_SELL_ORDER_RESPONSE_XEM,
        # ]
        # mock_create_order_expected_response = [
        #     CREATE_BUY_ORDER_RESPONSE_LSK,
        #     CREATE_BUY_ORDER_RESPONSE_DOGE,
        #     CREATE_BUY_ORDER_RESPONSE_XEM,
        # ] + sell_response_data
        # mock_create_order.side_effect = mock_create_order_expected_response
        #
        # topic_text_coins = ['LSK', 'XEM', 'DOGE']
        # coins_calculated = self.coinbase_effect_analyser.get_coins_potential_growth_calculated(
        #     topic_text_coins
        # )
        # coins_to_invest = self.coinbase_effect_analyser.get_coins_to_invest_in_proper_order(
        #     coins_calculated
        # )
        #
        # amount_to_invest = self.manager_transaction.amount_to_invest
        # amount_per_coin_expected = self.manager_transaction.get_amount_per_coin(
        #     amount_to_invest, len(coins_calculated)
        # )
        # for coin in coins_to_invest:
        #     transaction_coin = TransactionCoin(
        #         coin_object=coin,
        #         amount_to_invest=amount_per_coin_expected,
        #     )
        #     self.manager_transaction.perform_single_coin(transaction_coin)
        #
        # for i, transaction in enumerate(self.manager_transaction.coins_transaction):
        #     self.assertEqual(coins_calculated[i], transaction.coin_object)
        #
        #     transaction_buy = transaction.transaction_buy
        #     transaction_sell = transaction.transaction_sell
        #     quantity_ask_expected = SellManager.get_quantity_to_sell(
        #         quantity_ask=transaction_buy.quantity_ask
        #     )
        #     self.assertEqual(
        #         transaction_sell.price_filled,
        #         sell_response_data[i]['price']
        #     )
        #     self.assertEqual(
        #         transaction_sell.quantity_ask,
        #         quantity_ask_expected,
        #     )
        #     self.assertTrue(transaction_sell.is_success)
        #     self.assertEqual(
        #         transaction_sell.quantity_filled,
        #         sell_response_data[i]['origQty']
        #     )