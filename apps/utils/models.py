from django.db import models


# TODO: move this behaviour to some common module
class Timestampable(models.Model):
    """Automatically manage creation and update dates on model."""

    created_at = models.DateTimeField(
        auto_now_add=True,
        help_text='Creation date',
    )
    updated_at = models.DateTimeField(auto_now=True, help_text='Update date')

    class Meta:
        get_latest_by = 'updated_at'
        abstract = True