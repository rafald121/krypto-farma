import os
from celery import Celery
from celery.schedules import crontab

from api_binance.tasks import (
    save_prices_binance,
    save_marketcap_data,
    set_binance_coin_lots,
    set_binance_coin_min_notionals,
)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'krypto_farma.settings')

app = Celery('krypto_farma')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(minute='*/2'),  # each 2 minutes
        save_prices_binance,
    )
    sender.add_periodic_task(
        crontab(minute=0, hour='*/2'),  # each 2 hours
        save_marketcap_data,
    )
    sender.add_periodic_task(
        crontab(minute=0, hour=0),  # each midnight, once per day
        set_binance_coin_lots,
    )
    sender.add_periodic_task(
        crontab(minute=0, hour=0),  # each midnight, once per day
        set_binance_coin_min_notionals,
    )
