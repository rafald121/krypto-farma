import json

import requests
from binance.client import Client

client = Client(
    'z2SHMJd63nSgS1YYVtT4gRvKZO0caY0TQ32cRkKDWT8wWBUaHZHTxnZK0fgASxAC',
    'GgIiqzXhL4UuOpCmsxLHqE17SoUiF6kgVaXIbOCL7EqTn2k8XrMGaWk7o8IEXwhE'
)


def get_coins_up_levarage():
    all_coins = get_available_coins()
    all_coins_leverage = [coin for coin in all_coins if 'UP' in coin]
    return all_coins_leverage


def get_coins_down_levarage():
    all_coins = get_available_coins()
    all_coins_leverage = [coin for coin in all_coins if 'DOWN' in coin]
    return all_coins_leverage


def get_available_coins():
    response = requests.get('https://api.binance.com/api/v3/exchangeInfo')
    response_decoded = response.content.decode('utf-8')
    response_dict = json.loads(response_decoded)
    symbols = response_dict['symbols']
    coins = []
    for symbol in symbols:
        coins.append(symbol['baseAsset'])
    return list(set(coins))


def get_symbol_lots():
    response = requests.get('https://api.binance.com/api/v3/exchangeInfo')
    response_decoded = response.content.decode('utf-8')
    response_dict = json.loads(response_decoded)
    response_symbols = response_dict['symbols']
    symbols_lots = {}
    for symbol in response_symbols:
        key = symbol['symbol']
        symbols_lots[key] = get_lot_for_symbol_data(symbol)
    return symbols_lots


def get_symbol_min_notional():
    response = requests.get('https://api.binance.com/api/v3/exchangeInfo')
    response_decoded = response.content.decode('utf-8')
    response_dict = json.loads(response_decoded)
    response_symbols = response_dict['symbols']
    symbols_min_notionals = {}
    for symbol in response_symbols:
        key = symbol['symbol']
        symbols_min_notionals[key] = get_min_notional_for_symbol_data(symbol)
    return symbols_min_notionals


def get_lot_for_symbol_data(symbol_data):
    for filter in symbol_data['filters']:
        if filter['filterType'] == 'LOT_SIZE':
            return filter['stepSize']
    return None


def get_min_notional_for_symbol_data(symbol_data):
    for filter in symbol_data['filters']:
        if filter['filterType'] == 'PRICE_FILTER':
            return filter['tickSize']
    return None


def get_available_coins_price():
    prices = client.get_all_tickers()
    return {
        item['symbol']: item['price']
        for item in prices
    }
