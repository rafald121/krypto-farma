import logging
import time

from binance.exceptions import BinanceRequestException, BinanceAPIException
from celery import shared_task
from requests import RequestException

from coinbase_effect.services.redis_storage_manager import RedisStorageManager

logger = logging.getLogger(__name__)


@shared_task(
    autoretry_for=(BinanceRequestException, BinanceAPIException),
    retry_backoff=True,
    retry_backoff_max=1000
)
def save_prices_binance():
    time_start = time.time()
    RedisStorageManager().set_all_prices()
    time_finish = time.time() - time_start
    logger.info('Finished saving prices. Time: %s', time_finish)


@shared_task(
    autoretry_for=(RequestException, ),
    retry_backoff=True,
    retry_backoff_max=1000
)
def save_marketcap_data():
    logger.info('Start saving coin market cap data')
    time_start = time.time()
    RedisStorageManager().set_coinmarketcap_data()
    time_finish = time.time() - time_start
    logger.info('Finished saving coin market cap data. Time: %s', time_finish)


@shared_task(
    autoretry_for=(BinanceRequestException, BinanceAPIException),
    retry_backoff=True,
    retry_backoff_max=1000
)
def set_binance_coin_lots():
    logger.info('Start saving binance lots')
    time_start = time.time()
    RedisStorageManager().set_binance_coin_lots()
    time_finish = time.time() - time_start
    logger.info('Finished saving binance lots data. Time: %s', time_finish)


@shared_task(
    autoretry_for=(BinanceRequestException, BinanceAPIException),
    retry_backoff=True,
    retry_backoff_max=1000
)
def set_binance_coin_min_notionals():
    logger.info('Start saving binance lots')
    time_start = time.time()
    RedisStorageManager().set_binance_coin_min_notionals()
    time_finish = time.time() - time_start
    logger.info('Finished saving binance lots data. Time: %s', time_finish)
