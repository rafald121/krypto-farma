from django.db import models


# Create your models here.
class StrategyMode(models.Model):
    BESSA = 10
    HOSSA = 20

    choices = (
        (BESSA, 'Bessa'),
        (HOSSA, 'Hossa'),
    )

    mode = models.PositiveSmallIntegerField(choices=choices)
